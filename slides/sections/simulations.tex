% \subsection{Meso to macro paradigm}
\tikzsetfigurename{hdr_31-}

\begin{frame}{Behaviour of concrete: A story of scales}
    \begin{figure}
        \scalebox{1.0}{\input{tikz/concrete-scales.tex}}
    \end{figure}
    \vspace{-0.5em}
    \begin{columns}
        \begin{column}[T]{0.5\linewidth}
            \begin{mybboxt}{Models for structures}
                Complex phenomenological laws and simple variability.
            \end{mybboxt}
        \end{column}
        \onslide<2->{
            \begin{column}[T]{0.5\linewidth}
                \begin{mybboxt}{Mesomodels}
                    Heterogeneities endorse some of this complexity.
                \end{mybboxt}
            \end{column}
        }
    \end{columns}
    \onslide<3->{
        \begin{myrbox}
            How far can we go with phenomenological simplifications?
        \end{myrbox}
    }
\end{frame}

\tikzsetfigurename{hdr_32-}
\subsection{Kinematics enhancements}

\begin{frame}{Weak and strong discontinuities}
    % \vspace{-0.5em}
    % \begin{center}
    In this framework there are \textbf{two kinds} of discontinuities:
    % \end{center}
    \vspace{1em}
    \begin{columns}
        \begin{column}{0.5\linewidth}
            \begin{itemize}[leftmargin=*]
                \item {\color{myred} Material interface: weak discontinuity}
                \item {\color{myblue} Microcrack: strong discontinuity}
            \end{itemize}
            \vspace{0.5em}
            \par Standard FE discretisation is \textbf{kinematically enhanced}:\vspace{-0.5em}
            \[\boldsymbol\varepsilon = \boldsymbol B \boldsymbol d + {\color{myred}\boldsymbol G_\text{w} \left[ \left | \boldsymbol \varepsilon\right | \right ]} + {\color{myblue}\boldsymbol G_\text{s} \left[ \left | \boldsymbol u\right | \right ]} \]
        \end{column}
        \begin{column}{0.5\linewidth}
            \begin{mygbox}
                \small
                \par These enhancements fall into the {\bf Embedded Finite Element Method} by adding \textbf{local unkowns} \bibpersoshort{Oliver}{2006}:
                \vspace{-0.8em}
                \[{\color{myred}\left[ \left | \boldsymbol \varepsilon\right | \right ]} \ \ \text{and} \ \ {\color{myblue}\left[ \left | \boldsymbol u\right | \right ]}\]
                \vspace{-2em}
                \par in opposition to global enhancements like\\X-FEM \bibpersoshort{Moradi and Nazari}{2016}.
            \end{mygbox}
        \end{column}
    \end{columns}
    \vspace{0.5em}
    \onslide<2->{
        \begin{figure}
            \subfloat[Weak]{\scalebox{0.9}{\input{tikz/fe_weak_transformed}}}\hspace{1cm}
            \subfloat[Strong]{\scalebox{0.9}{\input{tikz/fe_strong_transformed}}}\hspace{1cm}
            \subfloat[Weak and strong]{\scalebox{0.9}{\input{tikz/fe_weak_strong_transformed}}}
        \end{figure}
    }
    % \vspace{-0.5em}
    % \bibperso{Roubin, E., Vallade, A., Benkemoun, N. et Colliat, J.-B.}{Multi-scale failure of heterogeneous materials: A double kinematics enhancement for Embedded Finite Element Method}{2015}
\end{frame}

\tikzsetfigurename{hdr_34-}
\subsection{Role of the phenomenology}

\begin{frame}{Simple VS complex phenomenology \hfill \raisebox{\dimexpr \baselineskip - \totalheight}{%
        \includegraphics[height=1.75em]{images/profile/yue}
        \includegraphics[height=1.75em]{images/profile/alex}
    }}
    \begin{center}
        $\underbrace{\text{\mybold{Simple}}}_\text{\tt\tiny [Roubin et.al., 2015]}$ vs $\underbrace{\text{\myboldr{Complex}}}_{\substack{\text{\tt\tiny [Sun et.al., 2021]}\\\text{\tt\tiny[Ortega et.al., 2021]}}}$ phenomenology
    \end{center}

    \begin{columns}
        \begin{column}[t]{0.5\textwidth}
            \begin{mybbox}
                \onslide<2->{\par \textbf{Localisation}: Rankine}
                \onslide<3->{\par \textbf{Opening}:
                \[ \Phi_\text{o} = \boldsymbol{\color{mypurple}n}\cdot\boldsymbol{\sigma}\cdot\boldsymbol{\color{mypurple}n} - \sigma_\text{y} \exp\left({-\frac{\sigma_\text{y}}{\mathcal{G}_\text{f}} {\color{myblue}[u]}}\right) \]}
                \onslide<4->{\par \textbf{Crack kinematics}: Mode I}
            \end{mybbox}
            % \vspace{1em}
            % \textbf{Question}
        \end{column}
        \begin{column}[t]{0.5\textwidth}
            \begin{myrbox}
                \onslide<2->{\par \textbf{Localisation}: +Morh-Coulomb}
                \onslide<3->{\par \textbf{Opening}: exponential}
                \onslide<4->{\par \textbf{Crack kinematics}: +Mode II / closure}
                \vspace{-1.0em}
                \begin{figure}
                    \onslide<2->{\subfloat{\includegraphics[height=0.3\textwidth]{images/LocalisationCriterion}}\hspace{1em}}
                    \onslide<4->{\subfloat{\includegraphics[height=0.3\textwidth]{images/2021-ortega-full-kinematics}}}
                \end{figure}
            \end{myrbox}
        \end{column}
    \end{columns}
    \vspace{1em}
    % How does the phenomenology improve the macroscopic behavior?
    \begin{itemize}[leftmargin=-1em]
        \item[] \bibperso{A. Ortega, E. Roubin, et. al.}{General Consistency of Strong Discontinuity Kinematics in Embedded Finite Element Method (E-FEM) Formulations}{2021}
        \item[] \bibperso{Y. Sun, E. Roubin, et. al.}{FE modeling of concrete with strong discontinuities for 3D shear fractures and comparison with experimental results}{2021}
        \item[] \bibperso{Y. Sun, E. Roubin, et. al.}{Strong discontinuity FE analysis for heterogeneous materials: The role of crack closure mechanism}{2021}
    \end{itemize}
\end{frame}

\begin{frame}{Simulations}
    \begin{itemize}
        \item<1->Simulation with \mybold{``real'' morphology} as the \insitu~tests\\[1em]
        \item<2->\mybold{Identification} of the parameters in tension
        \item<2->\mybold{Prediction} in compression at several confinement pressure\\[1em]
        \item<3->Comparison of the \mybold{macroscopic strain/stress} curves
        \item<3->Qualitative comparison of the \mybold{kinematics fields} and \mybold{crack patterns}\\[1em]
        % \item<4->Comparison between \mybold{simple} and \myboldr{complex} phenomenology
    \end{itemize}
\end{frame}

\begin{frame}{Prediction in compression for several confinement pressures \hfill \raisebox{\dimexpr \baselineskip - \totalheight}{%
        \includegraphics[height=1.75em]{images/profile/olga}
        \includegraphics[height=1.75em]{images/profile/eddy}
        \includegraphics[height=1.75em]{images/profile/yann}
    }}
    \vspace{-0.5cm}
    \begin{figure}
        \subfloat{\scalebox{1.0}{\input{tikz/stress_strain_alex_concrete_a}}}
    \end{figure}
    \vspace{-0.2cm}
    \bibperso{O. Stamati, E. Roubin, E. And\`o, Y. Malecot}{Fracturing process of micro-concrete under uniaxial and triaxial compression: Insights from in-situ X-ray mechanical tests}{2021}
\end{frame}
\begin{frame}{Prediction in compression for several confinement pressures \hfill \raisebox{\dimexpr \baselineskip - \totalheight}{%
        \includegraphics[height=1.75em]{images/profile/alex}
        \includegraphics[height=1.75em]{images/profile/olga}
    }}
    \vspace{-0.5cm}
    \begin{figure}
        \subfloat{\scalebox{1.0}{\input{tikz/stress_strain_alex_concrete_b}}}
    \end{figure}
    \vspace{-0.2cm}
    \bibperso{A. Ortega, E. Roubin, Y. Malecot, L. Daudeville}{Modelling of triaxial fracture processes in heterogeneous quasi-brittle materials using the Embedded Finite Element Method (E-FEM)}{2022}
\end{frame}

\begin{frame}{And so what?}
    \begin{columns}
        \begin{column}{0.65\linewidth}

            \vspace{0.5em}
            \begin{mybboxt}{Conclusions}
                \begin{itemize}[leftmargin=*]
                    \item<1->\mybold{Simple}: able to fairly reproduce the behaviour
                    \item<2->\myboldr{Complex}: improves the macroscopic behaviour
                \end{itemize}
            \end{mybboxt}
            \vspace{0.5em}
            \onslide<3->{
                \begin{myrbox}
                    How useful is it to complexify the model?
                \end{myrbox}
            }
            \vspace{0.5em}
            \onslide<4->{
                \begin{mygboxt}{Well, it's complicated}
                    \begin{itemize}[leftmargin=*]
                        \item Depends on the loadings
                        \item Loss of the mesomodel spirit
                        \item At the end of the day: \textbf{it's a story of scales}
                    \end{itemize}
                \end{mygboxt}
            }
        \end{column}

        \begin{column}{0.35\linewidth}
            \begin{figure}
                \subfloat[vol. strains]{\scalebox{0.3}{\input{tikz/tri05Mpa_volStrain}}}\hspace{1em}
                \subfloat[dev. strains]{\scalebox{0.3}{\input{tikz/tri05Mpa_devStrain}}}
                \caption*{\SI{5}{\mega\pascal}: Local DVC}
            \end{figure}\vspace{-2.5em}
            \begin{figure}
                \subfloat[Displ.]{\scalebox{0.3}{\input{tikz/5Mpa_ts33_disp}}}\hspace{1em}
                \subfloat[Cracks]{\scalebox{0.3}{\input{tikz/5Mpa_ts33_crackmat}}}
                \caption*{\SI{5}{\mega\pascal}: \mybold{Simple} E-FEM}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}[standout]
    \underline{E-FEM mesomodel}\\
    With heterogeneities and microcracking, global failure \visible<0>{\textit{ly}}mechanisms are modelled\only<2->{ \textit{fairly} decently}.\visible<0>{\textit{ly}}
\end{frame}

\begin{frame}{Perspectives on E-FEM}
    \begin{itemize}
        \item In relation to DVC
        \begin{itemize}
            \item<1->``Real'' boundary conditions (global DVC)
            \item<1->E-FEM/DVC quantitative comparison (displacements and crack patterns)
            \item<1->Parametrical identification (integrated DVC)
        \end{itemize}
        \item<2->Applications
        \begin{itemize}
            \item<2->Higher confinement pressures
        \end{itemize}
        \item<3->Statistical analysis
        \begin{itemize}
            \item<3->Morphological model
        \end{itemize}
    \end{itemize}
\end{frame}

