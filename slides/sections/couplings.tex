\subsection{Physical coupling}

\begin{frame}{Hydromechanical coupling \hfill \raisebox{\dimexpr \baselineskip - \totalheight}{%
    \includegraphics[height=1.75em]{images/profile/yue}
    \includegraphics[height=1.75em]{images/profile/jb}
}}
    \begin{mygbox}
        \textbf{Durability of concrete:} flow (pressure field) through porosity and cracks.\\
        \textbf{Scale:} mesoscale.\\
        \textbf{Saturation degree:} \myboldr{fully saturated}.

    \end{mygbox}
    \begin{mybbox}
        Make use of the crack representation of the E-FEM: ${\color{myblue}\left[ \left | \boldsymbol u\right | \right ]}$, $\color{mypurple} \bn$
        \par Leads to a \mybold{strong} physical coupling.
    \end{mybbox}
    \pause
    \begin{columns}
        \begin{column}[T]{0.25\linewidth}
            \begin{figure}
                \includegraphics[height=7em]{images/hm_mesh_full}%
                \caption*{Vertical crack}
            \end{figure}
        \end{column}

        \begin{column}[T]{0.25\linewidth}
            \begin{figure}
                \includegraphics[height=7em]{images/hm_displacement}%
                \caption*{Displacement field}
            \end{figure}
        \end{column}

        \begin{column}[T]{0.50\linewidth}
            \begin{figure}
                \only<2>{
                    \href{run:images/comp_HM_04-05.avi}{\includegraphics[height=7em]{images/comp_HM_04-05-0050.png}}%
                    \caption*{Pressure field for different crack sizes}
                }
                \only<3>{
                    \href{run:images/comp_HM_04-05.avi?autostart}{\includegraphics[height=7em]{images/comp_HM_04-05-0050.png}}%
                    \caption*{Pressure field for different crack sizes}
                }
            \end{figure}
        \end{column}

    \end{columns}
\end{frame}

\begin{frame}{Hydromechanical coupling}
    \begin{mygbox}
        \textbf{Durability of concrete:} damage due to drying shrinkage.\\
        \textbf{Scale:} nanoscale.\\
        \textbf{Saturation degree:} \myboldr{partially saturated}.
    \end{mygbox}
    \begin{mybboxt}{Stable state \bibperso{M. S. Hosseini}{PhD}{2015}}
        \begin{columns}[T]
            \begin{column}{0.3\linewidth}
                \onslide<2->{
                    \centering
                    1. Nanoporous network
                        \includegraphics[width=\textwidth]{images/mahban/_mahban_nanopores.png}
                    \vspace{-1em}
                    \footnotesize\color{mypurple} Morphological model (RF)
                }
            \end{column}
            \begin{column}{0.3\linewidth}
                \onslide<3->{
                    \centering
                    2. Water distribution \visible<0>{p}
                    \includegraphics[width=\textwidth]{images/mahban/_mahban_water_4.png}
                    \footnotesize Morphological filters (KL)
                }
            \end{column}
            \begin{column}{0.3\linewidth}
                \onslide<4->{
                    \centering
                    3. Mechanical behaviour\\[0.5em]
                        {\footnotesize Homogeneisation}\\
                        {\footnotesize \color{mypurple} E-FEM?}\\
                        {\footnotesize \color{mypurple} Adapted mesh?}\\
                        {\footnotesize \color{mypurple} Both?}
                }
            \end{column}
        \end{columns}
    \vspace{-0.5em}
    \end{mybboxt}
    \onslide<5->{
        \begin{myrboxt}{Transient state}
            Fluid mechanics: coupling with Finite Volume / Lattice-Boltzmann
        \end{myrboxt}
    }
\end{frame}


\begin{frame}[standout]
    \underline{Hydromechanical coupling}\\
    At the mesocale and the nanoscale
\end{frame}


\subsection{Domain decomposition}
\begin{frame}{Domain decomposition \hfill \raisebox{\dimexpr \baselineskip - \totalheight}{%
    \includegraphics[height=1.75em]{images/profile/rida}
    \includegraphics[height=1.75em]{images/profile/pierre}
    \includegraphics[height=1.75em]{images/profile/jb}
}}
    \begin{mygboxt}{Goal}
        Mesoscopic simulations at the scale of the \textbf{structure}.
    \end{mygboxt}
    \begin{myrboxt}{Problem}
        Requires to much \textbf{ressources}.
    \end{myrboxt}
    \pause
    \begin{mybboxt}{Domain decomposition}
        \vspace{-1em}
        \begin{columns}
            \begin{column}[]{0.35\linewidth}
                \begin{center}
                     \begin{figure}
                        \scalebox{0.6}{\input{tikz/dd_sketch_meshing_handout.tex}}
                     \end{figure}
                \end{center}
            \end{column}
            \begin{column}[]{0.65\linewidth}
                \textbf{Local/Global approach}
                    \begin{itemize}
                        \item \mybold{Integrated} Tight coupling between scales.
                        \item \mybold{Hybrid} Continuity of displacement and traction vectors.
                    \end{itemize}
            \end{column}
        \end{columns}
        \vspace{0.5em}
        \bibperso{O. Allix, P. Gosselet}{Non Intrusive Global/Local Coupling Techniques ... and Acceleration Techniques}{2020}
    \end{mybboxt}
\end{frame}

\begin{frame}{Global / local approach \hfill \raisebox{\dimexpr \baselineskip - \totalheight}{%
        \includegraphics[height=1.75em]{images/profile/rida}
        \includegraphics[height=1.75em]{images/profile/pierre}
        \includegraphics[height=1.75em]{images/profile/jb}
    }}
    \begin{figure}
        \scalebox{1.0}{\input{tikz/dd_handout}}
    \end{figure}
    \bibpersoshort{R. Grif}{ongoing PhD}
\end{frame}

\begin{frame}{Goals and questions \hfill \raisebox{\dimexpr \baselineskip - \totalheight}{%
    \includegraphics[height=1.75em]{images/profile/rida}
    \includegraphics[height=1.75em]{images/profile/pierre}
    \includegraphics[height=1.75em]{images/profile/jb}
}}

    \begin{description}
        \item[\color{myblue} Applications] Reasonable separation of scales
        \begin{itemize}
            \item Reinforced concrete structure
        \end{itemize}
        \item[\color{myblue} Challenges] ~
        \begin{itemize}
            \item Heterogenous quasi-brittle materials (E-FEM)
            \item Propagation of cracks between subdomains
        \end{itemize}
        \item[\color{myblue} Main questions]~
        \begin{itemize}
            \item Interpolation functions (linear / mortar)
            \item Optimal ratio between number of subdomains and patch coarseness
        \end{itemize}
    \end{description}
\end{frame}


\begin{frame}[standout]
    \underline{Domain decomposition}\\
    From the mesoscale to the structure.
\end{frame}

\subsection{Code coupling}

\begin{frame}{Code coupling}

    \begin{myrbox}
        The main technical issue with coupling approaches is the \textbf{communication protocol} between the different parties.
    \end{myrbox}

    \begin{columns}
        \begin{column}{0.5\linewidth}
            \begin{figure}
                \subfloat[\small HM coupling]{\scalebox{0.8}{\input{tikz/sketch_coupling_hydro.tex}}}\hfill
                \subfloat[\small Domain decomposition]{\scalebox{0.8}{\input{tikz/sketch_coupling_dd.tex}}}
            \end{figure}
        \end{column}
        \pause
        \begin{column}{0.5\linewidth}
            \begin{figure}
                \begin{mygboxt}{{\ttfamily CTL} middleware}
                    \begin{itemize}[leftmargin=*]
                        \item Synchronious / asynchronious
                        \item A posteriori knowledge of the protocol\\
                        {\scriptsize pipe/thread/tcp/mpi}
                    \end{itemize}
                \end{mygboxt}
            \end{figure}
        \end{column}
    \end{columns}
    \pause
    \begin{mybbox}
        \begin{multicols}{2}
            \begin{itemize}[leftmargin=*]
                \item Hydro-mechanical coupling\\[-0.5em]
                {\scriptsize (FE Mec. $\substack{\rightarrow\\[-1em] \leftarrow}$ FE Hyd.)}
                \item Domain decomposition\\[-0.5em]
                {\scriptsize (FE $\substack{\rightarrow\\[-1em] \leftarrow}$ FE $\substack{\rightarrow\\[-1em] \leftarrow}$ FE $\substack{\rightarrow\\[-1em] \leftarrow}$ $\dots$)}
                \item Integrated DVC\\[-0.5em]
                {\scriptsize (FE $\substack{\rightarrow\\[-1em] \leftarrow}$ DVC)}
                \item Hierarchical methods\\[-0.5em]
                {\scriptsize (FE$^2$, FEM$\times$DEM, docker, \dots)}
            \end{itemize}
        \end{multicols}
    \end{mybbox}
\end{frame}

\begin{frame}[standout]
    \underline{Good communication protocols}\\
    Focus on science and not technicality.
\end{frame}
