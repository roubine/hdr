\tikzsetfigurename{hdr_21-}

\subsection{Morphology}

\begin{frame}{Extraction of the morphology \hfill \raisebox{\dimexpr \baselineskip - \totalheight}{%
    \includegraphics[height=1.75em]{images/profile/olga}
    \includegraphics[height=1.75em]{images/profile/eddy}
    \includegraphics[height=1.75em]{images/profile/yann}
}}

\scalebox{1.0}{\input{tikz/segmentation_C}}
\vspace{-1.0em}
\bibperso{0. Stamati, E. Roubin, E. And\`o and Y. Malecot}{Phase segmentation of concrete x-ray tomographic images at meso-scale ...}{2018}
\end{frame}

\begin{frame}[standout]
    \underline{``Real'' morphology}\\Measured from images, used by the mesomodel.
\end{frame}

\begin{frame}{\insitu~tests \hfill \raisebox{\dimexpr \baselineskip - \totalheight}{%
    \includegraphics[height=1.75em]{images/profile/olga}
    \includegraphics[height=1.75em]{images/profile/eddy}
    \includegraphics[height=1.75em]{images/profile/yann}
}}
    \begin{figure}
        \includegraphics[height=0.9\textheight]{images/graph_c02_exp-Scans-2}
    \end{figure}
\end{frame}

\tikzsetfigurename{hdr_22-}
\subsection{Digital volume correlation}

% \begin{frame}{Registration: one point transformation}
%     \scalebox{1.0}{\input{tikz/registration_t01_handout}}
% \end{frame}

\begin{frame}{DVC: Digital Volume Correlation}
    The idea now is to use the same principle of \mybold{finding transformations} that matches the images to get a \mybold{displacement field}.
    \onslide<2->{
    \begin{columns}[t]
        \begin{column}{0.5\linewidth}
            \begin{mybboxt}{Local DVC}
                    Linear transformation on \mybold{several independent windows} of the image.
                    \begin{center}
                        \scalebox{0.5}{\input{tikz/regulargrid_t01}}
                    \end{center}
                    \vspace{-1em}
                    \bibperso{Stamati}{PhD}{2018}
            \end{mybboxt}
        \end{column}
        \begin{column}{0.5\linewidth}
            \begin{mygboxt}{Global DVC}
                \onslide<3>{
                    Use of a Finite Element mesh to transform the image.
                    \begin{center}
                        \includegraphics[width=\textwidth]{images/nagen2-displacements}
                    \end{center}
                    \vspace{-1em}
                    \par\bibperso{Besnard et.al.}{FE displacement ... digital image}{2006}
                }
            \end{mygboxt}
        \end{column}
    \end{columns}
    }
\end{frame}

\begin{frame}{A Finite Element mesh to transform an image}
    The paradigm behind the \mybold{global correlation} is to transform an image with a \mybold{displacement field} which support is a \mybold{Finite Element mesh} (instead of a set of linear operators $\F$).
    \vspace{1em}
    \begin{columns}
        \begin{column}{0.5\linewidth}
            \begin{figure}
                \scalebox{0.5}{\input{tikz/global_snow_short}}
            \end{figure}
            \[
                \Phi_\text{c}^2 = \sum_\Omega \frac{1}{2}\| g(\x + \u(\x)) - f(\x) \|_2
            \]
        \end{column}
        \begin{column}{0.5\linewidth}
            \onslide<3->{
                \begin{mybbox}
                    \begin{itemize}[leftmargin=*]
                        \item[$\color{mygreen}\boldsymbol{+}$] Intrinsic ``noise regularization''
                        \begin{itemize}[leftmargin=*]\small
                            \item Global problem
                        \end{itemize}
                        \item[$\color{mygreen}+$] One-to-one comparison with FE
                        \begin{itemize}[leftmargin=*]\small
                            \item Mechanically driven regularization
                            \item Easier FE/DVC coupling
                        \end{itemize}
                        % \item[$\color{myred}-$] Theoretically, no improvement
                    \end{itemize}
                \end{mybbox}
            }
        \end{column}
    \end{columns}
    \vspace{0.5em}
    \bibperso{F. Hild, S. Roux}{Comparison of Local and Global Approaches to Digital Image Correlation}{2012}
    \end{frame}

\begin{frame}{Global DVC}
    \begin{figure}
        \subfloat[$f$]       {\includegraphics[height=5cm,   valign=c]{images/V4-01-b1}}\hspace{1em}
        \subfloat[$g$]       {\includegraphics[height=5cm,   valign=c]{images/V4-02-b1}}\hspace{4em}
        \subfloat[Global DVC]{\includegraphics[height=5cm,   valign=c]{images/V4-global-dvc}}\hspace{0.5em}
        \subfloat            {\includegraphics[height=2.5cm, valign=c]{images/V4-global-dvc-scale}}
        \caption*{Some early results: triaxial tests on clay (SPAM benchmark)}
    \end{figure}
\end{frame}
\begin{frame}{Global DVC \hfill \raisebox{\dimexpr \baselineskip - \totalheight}{%
        \includegraphics[height=1.75em]{images/profile/florent}
    }}

    \begin{figure}
        \centering
        \only<1>{\includegraphics[width=\textwidth]{images/nagen2-displacements}\\[-1em]}
        \only<2>{\includegraphics[width=\textwidth]{images/nagen2-fluctuations}\\[-1em]}
        \caption*{Some early results: raw earth}
    \end{figure}
\end{frame}

\begin{frame}[standout]
    \underline{Experimental displacement field}\\Measured from a set of images, used as a comparison with the mesomodel.
\end{frame}

\tikzsetfigurename{hdr_23-}
\subsection{Multiple modalities}
\begin{frame}{Multiple modalities \hfill \raisebox{\dimexpr \baselineskip - \totalheight}{%
        \includegraphics[height=1.75em]{images/profile/olga}
        \includegraphics[height=1.75em]{images/profile/ale}
    }}

    It's possible to use \textbf{other kinds of sources} to see different things
    \begin{figure}
        \subfloat[\myboldr{X-Ray\\Material density}] {
            \begin{tikzpicture}
                \node[opacity=1.0, align=center, style={inner sep=0,outer sep=0}] at (0, 0) {
                    \includegraphics[width=5cm]{images/concrete_xr}
                };
                \node[opacity=1.0, align=center, style={inner sep=0,outer sep=0}] at (0, 0) {
                    \includegraphics<1->[width=5cm]{images/concrete_xr-1}
                };
            \end{tikzpicture}
        }\hspace{2em}
            \onslide<2->{
                \subfloat[{\mybold{Neutrons} from D50 (NeXT, ILL)\\}\mybold{Water}]{
                    \begin{tikzpicture}
                        \node[opacity=1.0, align=center, style={inner sep=0,outer sep=0}] at (0, 0) {
                            \includegraphics[width=5cm]{images/concrete_ne}
                        };
                        \node[opacity=1.0, align=center, style={inner sep=0,outer sep=0}] at (0, 0) {
                            \includegraphics<2->[width=5cm]{images/concrete_ne-1}
                        };
                    \end{tikzpicture}

            }
            }
    \end{figure}
    \onslide<2->{
        \par\bibperso{E. Roubin, E. And\`o, S. Roux}{The colours of concrete as seen by X-rays and neutrons}{2019}
    }
\end{frame}

\begin{frame}{Multimodal registration \hfill \raisebox{\dimexpr \baselineskip - \totalheight}{%
        \includegraphics[height=1.75em]{images/profile/eddy}
        \includegraphics[height=1.75em]{images/profile/roux}
    }}
    \begin{mygboxt}{The goal}
        Register two images with different modality (corresponding to the same state) in order to have \mybold{spatial consistency}.
    \end{mygboxt}

    \begin{myrboxt}{The issue}
        Classical algorithms are based on the minimisation of the \mybold{difference between two images}:
        \[\Func^2(f, g) = \frac{1}{2}(f - g)^2\]
        which doesn't make sense here with $f=\fx$ and $g=\fn$.
    \end{myrboxt}
\end{frame}

\begin{frame}{Multimodal registration \hfill \raisebox{\dimexpr \baselineskip - \totalheight}{%
        \includegraphics[height=1.75em]{images/profile/eddy}
        \includegraphics[height=1.75em]{images/profile/roux}
    }}
    \begin{mygboxt}{A solution}
        Construct a functional $\Func(\fx, \fn)$ based the joint histogram $p(\fx, \fn)$.
    \end{mygboxt}
    \vspace{-1em}
    \begin{figure}
        \onslide<1->{\subfloat[Joint histogram $p(\fx, \fn)$]  {\includegraphics[height=5cm,valign=t]{images/mmr-joint-histogram}}\hspace{2em}}
        \onslide<2->{\subfloat[Gaussian fit of $p(\fx, \fn)$]  {\includegraphics[height=5cm,valign=t]{images/mmr-joint-histogram-fitted}}\hspace{1em}}
        \onslide<1->{\subfloat                                 {\includegraphics[height=4cm,valign=t]{images/mmr-joint-histogram-scale.png}}}
    \end{figure}
\end{frame}
\begin{frame}{Multimodal registration \hfill \raisebox{\dimexpr \baselineskip - \totalheight}{%
        \includegraphics[height=1.75em]{images/profile/eddy}
        \includegraphics[height=1.75em]{images/profile/roux}
    }}
    With that in mind we can set
    \vspace{-2em}
    \[ \Func^2({\color{mygreen} \fx}, {\color{mygreen} \fn}) = \Func^2({\color{mygreen} \fxn}) = -\ln({\color{myred} p}({\color{mygreen} \fxn}))
    \approx \overbrace{\frac{1}{2}\min_i\left[ ({\color{mygreen} \fxn} - {\color{myred} \mean_i})^\text{T} ({\color{myred} \cov_i})^{-1} ({\color{mygreen} \fxn} - {\color{myred} \mean_i}) - \ln({\color{myred} \Phi_i})\right]}^\text{It's just a Gaussian}\]
    which minimisation leads to \mybold{narrower gaussians} \textit{i.e.} better alignment.
    \pause
    \vspace{-1em}
    \begin{figure}
        \onslide<2->{\subfloat[Not aligned]            {\includegraphics[height=4.5cm,valign=t]{images/mmr-joint-histogram-1}}}
        \onslide<3->{\subfloat[Eye registration]       {\includegraphics[height=4.5cm,valign=t]{images/mmr-joint-histogram-2}}}
        \onslide<4->{\subfloat[Multimodal registration]{\includegraphics[height=4.5cm,valign=t]{images/mmr-joint-histogram-3}}}
        \onslide<2->{\subfloat                         {\includegraphics[height=4cm,  valign=t]{images/mmr-joint-histogram-scale.png}}}
    \end{figure}
\end{frame}

\begin{frame}{Multimodal registration \hfill \raisebox{\dimexpr \baselineskip - \totalheight}{%
        \includegraphics[height=1.75em]{images/profile/eddy}
        \includegraphics[height=1.75em]{images/profile/roux}
    }}
    \begin{columns}
        \begin{column}{0.5\linewidth}
            \begin{figure}
                \includegraphics[width=\textwidth]{images/mmr-checkerboard}
                \caption*{Good alignment of the two modalities}
            \end{figure}
        \end{column}
        \begin{column}{0.5\linewidth}
            \begin{mybboxt}{Going further}
                Combined image correlation
                \begin{itemize}[leftmargin=*]
                    \item Noise reduction
                    \item Measure of the kinematics that map one phase to the next image
                \end{itemize}
            \end{mybboxt}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}[standout]
    \underline{Water distribution}\\Measured from neutrons, useful for physical coupling.
\end{frame}


\begin{frame}[fragile]{Software development \hfill \raisebox{\dimexpr \baselineskip - \totalheight}{%
        \includegraphics[height=1.75em]{images/profile/olga}
        \includegraphics[height=1.75em]{images/profile/eddy}
    }}
    \vspace{-0.5em}
    \begin{center}
        \mybold{S}oftware for \mybold{P}ratical \mybold{A}nalysis of \mybold{M}aterials
    \end{center}
    % \vspace{1em}
    \begin{columns}[T]
        \begin{column}{0.5\linewidth}
            {\small Command lines}
            \begin{lstlisting}[language=bash]
 spam-ldic -tsv -tif -glt 20000 \
    -PhiFile V4-registration.tsv \
    -hws 10 V4-01.tif V4-02.tif\end{lstlisting}
            % \pause
            {\small Scripts}
            \begin{lstlisting}[language=python]
 import spam.DIC
 import spam.dataset

 # load images
 f = spam.datasets.loadV4one()
 g = spam.datasets.loadV4two()

 # registration
 reg = spam.DIC.registration(f, g)\end{lstlisting}
            % \pause
            {\small Documentation}\\[-0.5em]
            {\tiny \url{https://ttk.gricad-pages.univ-grenoble-alpes.fr/spam}}
        \end{column}
        % \pause
        \begin{column}{0.5\linewidth}
            \begin{itemize}[leftmargin=*]
                \item Publication in June 2020: 43 citations\\[0.5em]
                \includegraphics[width=0.8\linewidth]{images/logo-joss}\hfill
                \item Python Package Index\\[0.5em]
                \begin{columns}[c]
                    \begin{column}{0.1\linewidth}
                        \includegraphics[height=4em]{images/logo-pypi}
                    \end{column}\hfill
                    \begin{column}{0.8\linewidth}
                        18k downloads last 6 months
                    \end{column}
                \end{columns}
                \item Anaconda\\[0.5em]
                \begin{columns}[c]
                    \begin{column}{0.1\linewidth}
                        \includegraphics[height=3em]{images/logo-conda}
                    \end{column}\hfill
                    \begin{column}{0.8\linewidth}
                        1k downloads last 6 months
                    \end{column}
                \end{columns}
            \end{itemize}
        \end{column}
    \end{columns}
    \vspace{0.5em}
    \bibperso{Stamati, And\`o, Roubin, et. al.}{SPAM: Software for Practical Analysis of Materials Journal Of Open Source Software}{2020}
\end{frame}

