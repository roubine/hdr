#!/bin/bash

echo "Bash version ${BASH_VERSION}..."

rm _*.png
for f in *.png; do
    echo $f
#     convert $f \
#         -gravity Center \
#         \( -size 2560x2560 \
#         xc:Black \
#         -fill White \
#         -draw 'circle 1150 1150 1150 400' \
#         -alpha Copy \
#         \) -compose CopyOpacity -composite \
#         -trim -resize 750x750 $f-bin2.png
#    convert $f -crop 1000x562+0+219 _$f
    convert $f -crop 1000x500+0+250 _$f
done

# ffmpeg -framerate 1 -pattern_type glob -i '_mahban_water*.jpg' -c:v libx264 -pix_fmt yuv420p out.mp4
# ffmpeg -framerate 1 -pattern_type glob -i '_mahban_water*.jpg' -c:v ffv1 -pix_fmt yuv420p out.avi
