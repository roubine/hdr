import numpy

f = "t01-exp"

a = numpy.loadtxt(f+".txt")

print a.shape

tabx = numpy.mean(a[:, 0].reshape(-1, 10), axis=1)
taby = numpy.mean(a[:, 1].reshape(-1, 10), axis=1)


with open(f+"-smoothed.txt", "w") as w:
    for x, y in zip(tabx, taby):
        w.write("{}\t{}\n".format(x, y))
