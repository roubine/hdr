# Habilitation à diriger les recherches
[![build artifacts](https://img.shields.io/gitlab/pipeline-status/roubine/hdr?gitlab_url=https%3A%2F%2Fgricad-gitlab.univ-grenoble-alpes.fr&label=Compilation&logo=latex&style=for-the-badge)](https://gricad-gitlab.univ-grenoble-alpes.fr/roubine/hdr/-/pipelines)
[![Downloads](https://img.shields.io/gitlab/v/tag/roubine/hdr?gitlab_url=https%3A%2F%2Fgricad-gitlab.univ-grenoble-alpes.fr&label=Télécharger&sort=date&style=for-the-badge)](https://gricad-gitlab.univ-grenoble-alpes.fr/roubine/hdr/-/jobs/artifacts/main/browse?job=pdf)
