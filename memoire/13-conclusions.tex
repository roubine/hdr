\section{Réflexions et perspectives}
\label{sec:reflexions}

\begin{sidenotes}
  \par Dans cette dernière section, je conclue le rapport sur mes recherches effectuées en apportant des réflexions quant aux résultats présentés et les perspectives liées, mais aussi en décrivant brièvement d'autres aspects de mes recherches que j'ai sciemment éludées jusque là.
\end{sidenotes}

\subsection{Les faciès de fissuration}
\par Une problématique primordiale que pose la méthodologie présentée ici est la définition rigoureuse d'un faciès de fissuration macroscopique.
Cette problématique est toujours d'actualité et doit faire partie  des réflexions futures du développement de la méthode dans sa globalité, notamment vis-à-vis d'une intégration dans des modèles multiéchelles ou multiphysiques macroscopiques.
Je reviens donc dans cette section sur la définition que l'on en fait à ce jour, puis en la comparant à d'autres travaux que j'ai effectués lors de mon postdoc \cite{2015:oliver:continuum}, je donne mon point de vue sur les différentes solutions pouvant être mises en place pour aborder ce problème.

\subsubsection{Le paradigme des définitions micro et macro}
Il est commun de se représenter une fissure comme une variété bidimensionnelle définie dans un espace tridimensionnel.
En attribuant un champ vectoriel d'ouverture (en mm), on peut ainsi localiser et définir une fissure pouvant être utilisée dans une variété de modèles.
C'est ce qui est fait à l'échelle fine dans le cadre des E-FEM définies ici.
Une microfissure est en effet définie comme un plan géométriquement situé dans l'espace (position et orientation) auquel on attribue une ouverture (en mm), elle-même orientée.
Notre méthode de résolution ne contraint pas explicitement le mécanisme de création et d'ouverture de ces fissures par rapport à leur voisinage afin de garder un aspect local et simple au problème.
Grâce à la simplicité et la légèreté de la méthode que confère ce choix, il est possible d'atteindre des discrétisations avec la finesse nécessaire à la représentation des hétérogénéités.
Cependant cela se fait au détriment d'une transcription évidente de la définition de la fissure à l'échelle macroscopique, notamment dû au fait que la continuité des fissures n'est pas imposée.
C'est donc la coalescence des microfissures qui définit notre faciès de fissuration macroscopique.
Techniquement, cela correspond à l'ensemble des éléments ayant initié une discontinuité forte, ce qui, en général, produit une forme géométrique complexe à analyser.
En effet, même pour un essai de traction simple (\figref{fig:2020-stamati-crack-evolution}), la fissure macroscopique principale, bien qu'évidente à repérer à l'œil nu, n'est pas clairement définie d'un point de vue géométrique.

\subsubsection{Correspondance avec l'imagerie}
\par A ce stade il est tout de même intéressant de remarquer que la problématique est semblable à celle que l'on retrouve en analysant des tomographies (\figref{fig:2018-stamati-correlation-tension}).
Dans ce domaine, la littérature nous apporte plusieurs approches (notamment \cite{2003:landis:microstructure}) qui ont fait l'objet de travaux préliminaires non publiés lors de la thèse d'Olga Stamati.
Le verrou principal ici est de trouver une méthode efficace pour traiter des faciès de fissuration de type compression simple dont la complexité topologique est plus importante.

\subsubsection{Des alternatives}
\par Une solution qui permet de transposer directement la définition microscopique d'une fissure à l'échelle macroscopique est d'imposer la continuité des microfissures.
Sans nécessairement changer de méthode (comme par exemple les X-FEM), il est possible d'imposer cette continuité, soit avec des algorithmes ``locaux'' (imposition de la continuité entre voisins), soit avec des algorithmes globaux (résolution d'un problème annexe définissant un chemin de fissure global \figref{fig:2015-oliver-crackpathfield}).
Les deux problèmes majeurs auxquels on peut s'attendre avec la mise en place de ces méthodes sont:
\begin{enumerate}
  \item l'augmentation de la complexité du calcul global, que ce soit par une méthode élémentaire plus lourde nécessitant de boucler sur les voisins, soit par l'ajout d'un problème global à chaque pas de temps.
  \item l'augmentation de la complexité de la représentation géométrique des microfissures, car des sections de plans ne sont plus suffisants pour assurer une continuité en 3D.
\end{enumerate}
À temps de calcul équivalent, il semble difficile d'obtenir une discrétisation assez fine avec ces méthodes afin de représenter objectivement les hétérogénéités.

\begin{figure}[!ht]
  \centering
  \includegraphics[height=5cm]{media/efem/2015-oliver-crackpathfield}
  \caption{
      Issu de mon postdoc \cite{2015:oliver:continuum}: continuité de la fissure dans le cadre des E-FEM.
      En bleu: définition géométrique du chemin de fissuration issu d'un problème annexe basé sur l'état de déformation à chaque pas de temps.
      En vert: l'orientation des discontinuités fortes.
  }
  \label{fig:2015-oliver-crackpathfield}
\end{figure}


\subsubsection{Conclusion}
\par On se retrouve donc ici avec un compromis entre la qualité du modèle mésoscopique et de la représentation de la morphologie.
Mon point de vue est pour le moment de garder la philosophie de simplicité du modèle local (\ie, de favoriser la qualité de la morphologie) et de continuer à explorer des moyens d'analyser les faciès complexes afin de quantifier leurs caractéristiques géométriques et topologiques.

\subsection{Le futur du modèle}
\par D'un point de vue mécanique, avec les avancements effectués lors de la thèse d'Alejandro Ortega (généralisation des modes de ruptures locaux), on peut considéré le modèle à un état de maturité suffisante pour envisager deux chemins d'investigations que je vais détailler ici: 1) l'utilisation du modèle pour des sollicitations plus extrêmes et 2) le couplage du modèle avec des phénomènes thermo, hydro et/ou chimique.

\subsubsection{Compression confinée}
\par Les résultats en compression confinée illustrés \figref{fig:2021-ortega-macro} montrent que le modèle a la capacité de capturer la complexité des phénomènes de rupture qui interviennent lors de compression confinée.
Une première étape serait d'effectuer une identification plus rigoureuse (partie sciemment éludée lors de la thèse d'Alejandro) des différents paramètres du modèle sur une banque d'essais \insitu plus riche pour les mêmes ordres de confinement (\SI{\approx 10}{\mega\pascal}).
\par Une deuxième étape serait de pousser les pressions de confinement aux ordres obtenus par le presse Giga du laboratoire (\SI{650}{\mega\pascal}) qui font apparaitre des faciès de fissuration non ordinaires (succession de macrofissures horizontales) \cite{2012:poinard:compression,2014:piotrowska:experimental}.
Il serait en effet intéressant d'identifier quels sont les mécanismes locaux responsables de ces modes de rupture.

\subsubsection{Couplage multiphysique}
\par Les interactions entre les différents phénomènes physiques qui interviennent dans les matériaux cimentaires font l'objet de nombreuses études expérimentales et numériques \cite{2005:benboudjema:interaction,2014:buffo-lacarriere:application}.
Contrairement à de nombreux chercheurs dans le domaine de la durabilité des bétons, je ne suis pas un expert des différentes lois physiques en jeu.
Cependant, je vais décrire ici comment le cadre numérique mis en place dans mes travaux pourrait apporter des résultats intéressants, même avec une utilisation rudimentaire de ces lois.

\vs\par La position, l'orientation ainsi que l'ouverture des microfissures sont des grandeurs qui ressortent naturellement d'un calcul mécanique avec les E-FEM.
Cette information possède un sens physique intrinsèque qui mène logiquement à l'idée de couplage avec d'autres phénomènes physiques.
Les effets thermiques sont des phénomènes étudiés depuis longtemps et relativement bien connus.
Pour les types de matériaux qui nous intéressent, on peut considérer que le couplage est unidirectionnel (la thermique impacte la mécanique).
Les phénomènes hydriques jouent également un rôle primordial dans le comportement des bétons (retrait dû au séchage) et leur durabilité.
Cependant le couplage est plus compliqué à mettre en œuvre dans le sens où cette fois-ci les effets hydriques sont impactés par la mécanique.
C'est pour cette raison que je m'attarde ici sur le couplage hydromécanique.

\vs\par On distingue deux niveaux de physique qui permettent de coupler deux modèles. Les \emph{couplages faibles} où la communication entre les modèles ne se fait que dans un sens et l'équilibre global du système n'est pas assurée et les \emph{couplages forts} où la communication entre les modèles se fait dans les deux sens et l'équilibre global du système est assurée.

\vs\par Le couplage faible est le plus facile et le moins intrusif à mettre en place.
Le principe est que les ``sorties'' du premier modèle servent de ``données d'entrée'' au second modèle.
Dans le cadre hydromécanique, il est généralement entendu que c'est le champ de pression issue d'un calcul d'équilibre hydrique (perméabilité) qui est utilisé dans un calcul mécanique via une loi de comportement poromécanique du type:
\begin{equation}
  \boldsymbol{\sigma} = \boldsymbol{C}:\left(\boldsymbol{\varepsilon} + \boldsymbol{G}_\text{s}\bub\right) - b (p-p_0) \boldsymbol{\mathbb{I}}
\end{equation}
où $\boldsymbol{G}_\text{s}\bub$ représente la microfissure (et son support), $p$ la pression et $b$ le coefficient de biot.
On modélise donc une pression qui engendre une contrainte additionnelle à une loi de comportement ``classique'' lors de la résolution du problème mécanique.
Cependant, et c'est là que le terme ``faible'' prend son sens, les contraintes et déformations n'ont pas d'impact sur la pression (voir \figref{fig:coupling:weak}).
On n'atteint donc jamais un équilibre global pour les deux problèmes\footnote{Ce qui ne signifie pas que la résolution soit nécessairement explicite.}.
L'intérêt que je vois à appliquer cette stratégie dans le cadre des E-FEM est d'effectuer les deux calculs hydrique et mécanique sur des matériaux hétérogènes.
On peut en effet appliquer le principe des discontinuités faibles pour le calcul hydrique, permettant ainsi de gérer les interfaces entre les phases.

\vs\par La version \emph{forte} du couplage implique que la mécanique ait à son tour un effet sur la partie hydrique.
Dans notre cadre, le calcul mécanique engendre des microfissures $\bub$ qui ont un impact sur le calcul hydrique.
D'une part, en suivant les développements de \cite{2011:lagier:numerical}, le tenseur de perméabilité (liant le gradient de pression à la pression) est définit comme suit:
\begin{equation}
  \boldsymbol{k} = \boldsymbol{k}_0 + \frac{\bub^3}{12\mu h_\text{m}}(\boldsymbol{\mathbb{I}} - \n\otimes\n)
\end{equation}
où $\bub$ représente toujours la microfissure et $\n$ son orientation.

\vs\par Au sein des couplages forts on peut également hiérarchisez les méthodes de résolution.
\begin{description}
  \item[Résolution monolithique] Les équations hydriques et mécaniques sont résolues dans le même système ce qui permet d'atteindre l'équilibre global en utilisant des méthodes de résolutions classiques.
  Cependant, même si cette approche reste la plus intuitive, elle entraîne certains problèmes comme l'augmentation de la taille du système global ainsi qu'un aspect intrusif et très spécifique en termes de développement.
  \item[Résolution partitionnée] Les résolutions partitionnées consistent à séparer les deux solveurs hydrique et mécanique, apportant un aspect modulaire à la méthode.
  Au cours d'un même incrément de chargement, les deux solveurs sont appelés plusieurs fois et dialoguent ensemble afin d'atteindre, au cours d'un processus itératif, l'équilibre global du problème.

  Sans entrer dans les détails des différentes implémentations possibles de cette méthode, l'idée est d'avoir deux méthodes de résolution indépendantes pour les problèmes hydriques et mécaniques et de les faire dialoguer ensemble au sein d'un même pas de temps jusqu'à la convergence (voir \figref{fig:coupling:strong}).
  L'inconvénient évident est la présence d'un algorithme itératif supplémentaire à la résolution non linéaire du problème mécanique.
  Cependant il doit être comparé aux avantages que la méthode confère comme:
  \begin{itemize}
    \item l'absence de nécessité de gérer numériquement des variables qui possèdent de nombreux ordres de grandeur de différence (dans le système des unités SI),
    \item la préservation du nombre de degrés de liberté par rapport à un modèle mécanique classique,
    \item la possibilité de choisir différentes méthodes de résolution et différentes discrétisations pour les deux modèles,
    \item la possibilité d'utiliser différents logiciels et matériels de calcul.
  \end{itemize}
\end{description}

\begin{figure}
  \centering
\end{figure}


\begin{figure}[!ht]
  \centering
  \begin{subfigure}[t]{0.45\textwidth}
    \centering
    \scalebox{1.0}{\input{media/efem/coupling-weak}}
    \caption{Couplage faible: la communication au sein d'un pas de temps ne se fait que de l'hydraulique vers la mécanique: {\color{mygray}{$(2)$}}.}
    \label{fig:coupling:weak}
  \end{subfigure}
  ~
  \begin{subfigure}[t]{0.45\textwidth}
    \centering
    \scalebox{1.0}{\input{media/efem/coupling-strong-partitioned}}
    \caption{Couplage fort: exemple d'un schéma implicite de résolution partitionnée.
    La communication se fait dans les deux sens: {\color{mygray}{$(2)$}} et {\color{mygray}{$(4)$}}.}
    \label{fig:coupling:strong}
  \end{subfigure}
  \caption{
    Schéma illustrant les différences entre couplage faible et couplage fort.
    En noir: évolution ``temporelle'' des deux calculs (pas de temps globaux). $(i)$: ordre des étapes au sein d'un pas de temps global. En bleu: calculs hydrauliques. En rouge: calculs mécaniques. En gris: transferts d'information.
  }
  \label{fig:coupling}
\end{figure}


\par L'aspect modulaire des méthodes partitionnées est très attractive dans le cadre des travaux que j'envisage dans le sens où elles offrent une flexibilité qui permet de l'appliquer à d'autres types de problèmes comme la corrélation d'image intégrée, où un problème de corrélation d'image est couplé à un problème mécanique (section \ref{subsec:reflexions:dvc} \ref{subsubsec:reflexions:dvc:integrated}) ou la décomposition de domaines où un solveur global est couplé avec une multitude de sous problèmes (chapitre \ref{chap:projects} section \ref{sec:domain-decomposition}).

\subsection{Mécanique et corrélation d'image}
\label{subsec:reflexions:dvc}
\par Depuis mon arrivée au laboratoire 3SR, un nouvel aspect de mes recherches a été de faire dialoguer les méthodes de types E-FEM et des méthodes expériementales, notamment via l'imagerie 3D \cite{2020:stamati:impact}.
Il est important de noter que au vu du challenge qu'impose la corrélation d'image 3D sur des éprouvettes en béton à cette échelle, les outils présents au laboratoire \cite{2017:tudisco:tomowarp2:} ne permettaient pas d'obtenir des résultats concluants, le bruit étant trop important par rapport aux déplacements à mesurer.
Au même moment, il y avait également une limitation des outils existants pour des applications dans des milieux granulaires.
C'est la conjonction de ces deux besoins, ainsi que du début de la thèse de Olga Stamati, qui a permi de faire naitre un projet de logiciel open source {\tt SPAM: Sofware for Practical Analysis of Materials} \cite{2020:stamati:spam:}, dont je suis un des principaux développeurs.

\par Vu que cela correspond à une partie non négligeable de mes travaux de recherche ses 7 dernières années, je vais ici décrire, dans un premier temps les évolutions à venir du logiciel, puis, la philosophie open source que nous appliquons à ce projet.

\subsubsection{Correlation d'image globale: la mécanique au service de la régularisation}
\par La première méthode de corrélation d'image que nous avons implémentée est dite la méthode ``locale'' où, chaque point du champ de déplacement est obtenu à l'aide du recalage d'une ``imagette'' \figref{fig:2020-stamati-localdvc}.
Le compromis se joue sur la taille des imagettes qui d'un côté est imposée par le bruit de l'image et de l'autre la discrétisation du champ exigée.
Cette méthode est relativement intuitive, mais pose différents problèmes comme le recouvrement potentiel des imagettes et l'analyse a posteriori du champ obtenu.
\begin{figure}[!ht]
  \centering
  \includegraphics[width=0.8\linewidth]{media/tomo/2020-stamati-localdvc}
  \caption{
      Issu de \cite{2020:stamati:impact}: principe de la méthode de corrélation d'images dite ``locale''.
  }
  \label{fig:2020-stamati-localdvc}
\end{figure}

\par Il existe une approche alternative dite ``globale'' \cite{2006:besnard:finite-element, 2012:hild:comparison} qui, dans le cadre d'un dialogue avec des méthodes E-FEM, semble plus pertinente.
Les méthodes globales consistent à utiliser un maillage de type Élément-Finis comme support du champ de déplacement $\bu(\x)$.
En supposant un opérateur qui déforme une image à partir d'un tel champ, il est possible d'écrire la minimisation du résidu entre l'image de référence $f$ et la déformée $g$:
\begin{equation}
  \eta(\x) = g(\x + \bu(\x)) - f(\x)
  \label{eq:global-dvc:residu}
\end{equation}
sous une forme semblable à celle d'un problème EF non linéaire (schéma de Newton-Raphson) :
\begin{equation}
  \boldsymbol{M}_\text{c}\dbu = \bb
  \label{eq:global-dvc:newton}
\end{equation}
où $\boldsymbol{M}_\text{c}$ est une matrice assemblée qui contient, au niveau élémentaire, des informations relatives au gradient de l'image, $\dbu$ est un vecteur contenant l'incrément de déplacement aux noeuds et $\bb$ le résidu.

\par Théoriquement cette méthode n'assure pas de meilleurs résultats que la méthode locale.
On retrouve d'ailleurs le même compromis entre la taille des imagettes et la finesse du maillage.
Cependant elle confère plusieurs avantages comme une régularisation ``naturelle'' due à la résolution globale de \eqref{eq:global-dvc:residu} via \eqref{eq:global-dvc:newton}, là où chaque composante du champ était calculée de façon indépendante avec des méthodes locales.
Comme illustré \figref{fig:2020-stamati-globaldvc}, on voit qu'il est aisé de comparer des champs issus de calculs EF et de corrélation d'image.
En effet, sous réserve d'utiliser la même discrétisation, il n'y a plus besoin de passer par une phase d'interpolation.

\begin{figure}[!ht]
  \centering
  \includegraphics[width=0.8\linewidth]{media/tomo/2020-stamati-globaldvc}
  \caption{
      Issu de \cite{2020:stamati:impact}: Comparaison entre un calcul E-FEM et un résultat de corrélation d'image ``globale''.
  }
  \label{fig:2020-stamati-globaldvc}
\end{figure}


\par Cependant il est généralement admis dans la communauté que les méthodes globales nécessitent une régularisation supplémentaire afin de limiter l'impact du bruit des images sur le champ de déplacement obtenu, conclusion que je partage suite au développement de la méthode.
La solution généralement adoptée est une régularisation de type Tikhonov \cite{1966:morozov:regularization} qui permet d'atténuer les hautes fréquences du champ de déplacements issu du bruit.
L'inconvénient principal reste que la matrice de régularisation ne possède pas de sens physique (généralement proportionnelle à l'identité).
Il est cependant possible de construire une matrice en partant du principe que le matériau possède un comportement élastique, conférant ainsi à la régularisation un sens mécanique.
En suivant la méthode de l'equilibrum gap \cite{2004:claire:a}, la régularisation se base sur la différence entre la solution et une solution qui satisfait les équations d'équilibre d'un milieu élastique.
En suivant les travaux récents de \cite{2019:mendoza:complete}, il est également possible d'ajouter un terme de régularisation des surfaces qui minimise le gradient de vecteur contrainte sur les surfaces de Dirichlet.
Au final, le problème à résoudre est de la forme:
\begin{equation}
  (\boldsymbol{M}_\text{c} + \boldsymbol{M}_\text{reg})\dbu = \bb - \boldsymbol{M}_\text{reg} \bu
\end{equation}
avec
\begin{equation}
  \boldsymbol{M}_\text{reg} = \boldsymbol{M}_\text{m} + \sum_{\mathcal{N}_i} \boldsymbol{M}_{\mathcal{S}_i}
\end{equation}
où $\boldsymbol{M}_\text{m}$ est proportionnelle à une matrice de rigidité de type EF et $\boldsymbol{M}_{\mathcal{S}_i}$ une matrice spécifique aux conditions aux limites de Dirichlet qui minimise $\nabla\sigma\cdot\bn$.

\vs

\begin{sidenotes}
  Ces régularisations sont en cours de développement dans le logiciel {\tt SPAM} \cite{2020:stamati:spam:} et donnent déjà des résultats encourageants.
\end{sidenotes}

\subsubsection{Corrélation d'image intégrée: la corrélation au service de l'identification de modèle}
\label{subsubsec:reflexions:dvc:integrated}
\par La régularisation basée sur la mécanique n'est pas le seul avantage que confèrent les méthodes globales.
Les similitudes entre les jeux de données d'un problème EF et d'une corrélation globale permettent plus aisément (\ie, sans interpolations) de coupler les deux.
Attention cependant, ici le paradigme change complètement et la mécanique n'est plus ``au service'' de corrélation comme pour les problématiques de régularisation.
Ici, le couplage est, à l'instar des couplages multiphysiques, à comprendre comme un dialogue entre un calcul EF et une corrélation d'image. Il peut être fait d'une façon faible ou forte.
Le paradigme est différent dans le sens où l'objectif ici n'est pas d'améliorer la corrélation d'images, mais de minimiser la différence entre les deux afin d'identifier les paramètres du modèle EF.

\par Une ébauche de couplage faible a été effectuée à la thèse d'Olga Stamati où, les conditions aux limites d'un essai issues de la corrélation d'image étaient imposées à des calculs EF dont on faisait changer les paramètres élastiques.
Comme illustré \figref{fig:2020-stamati-integrateddvc}, on obtient une surface d'erreur qui permet de trouver un minimum pour un certain jeu de propriétés matériaux.

\begin{figure}[!ht]
  \centering
  \includegraphics[width=0.6\linewidth]{media/tomo/2020-stamati-integrateddvc}
  \caption{
      Issu de \cite{2020:stamati:impact}: couplage faible entre calculs E-FEM et corrélation d'image globale.
  }
  \label{fig:2020-stamati-integrateddvc}
\end{figure}

\par Ces résultats sont encore provisoires. En effet, étant basées sur des méthodes de corrélations d'images mal régularisées, les conditions aux limites imposées ne permettent pas de déduire un jeu de matériaux pertinent.
C'est d'ailleurs ce constat qui a initié le développement des méthodes de régularisation mentionnées ci-dessus.

\vs\par À l'instar d'autres couplages, il est également possible d'opérer un couplage fort entre les deux méthodes \cite{2006:hild:digital}.
Ce couplage est plus intrusif, mais permet, entre autres, d'automatiser la méthode mise en place par Olga en déterminant une direction de recherche pour la minimisation des paramètres matériaux.
À ma connaissance, aucun couplage fort avec les E-FEM n'a été publié, cependant, l'aspect local des E-FEM et la similitude entre les discontinuités fortes et les fissures observées sur les images promettent une intégration intéressante comme la possibilité de mesurer les ouvertures des fissures.

\subsection{Les aspects morphologiques et l'analyse statistique}

\begin{sidenotes}
  \par Pour conclure cette partie sur mes réflexions et le chapitre sur les travaux effectués je comptais revenir sur un des deux piliers de ma thèse qui traitait d'un modèle de génération de morphologie basée sur des champs aléatoires corrélés.
  Il n'est pas mention de cette partie de ma recherche jusque-là pour deux raisons.
  Premièrement, mes apports à cette thématique n'ont fait l'objet que d'une seule publication \cite{2016:roubin:critical}.
  Et deuxièmement, ces apports ont un aspect fondamental qu'il était difficile d'inscrire naturellement dans ce chapitre.
  \par Cependant le potentiel de ce modèle est important par rapport aux thématiques que j'aborde et c'est pourquoi je voulais le mentionner ici.
\end{sidenotes}

\subsubsection{L'intérêt d'un modèle morphologique}
\par Lors de ma thèse j'ai développé un modèle morphologique permettant de générer des morphologies basées sur des excursions de champs aléatoires corrélés afin de représenter les hétérogénéités du béton à l'échelle mésoscopique \cite{2015:roubin:meso-scale}.

\par La tomographie permet de récupérer des morphologies réalistes \cite{2018:stamati:phase} afin de calibrer le modèle sur un nombre d'échantillons restreint.
Puis, la capacité du modèle à générer des morphologies aléatoires représentatives statistiquement équivalentes permet d'ouvrir la porte aux études statistiques qui, à terme, permettraient de définir des volumes élémentaires représentatifs pour diverses propriétés mécaniques telles que les résistances en compréhension ou en traction.

\subsubsection{Les limitations actuelles}
\par Une des originalités du modèle était de pouvoir prédire les caractéristiques géométriques et topologiques de la morphologie en fonction des caractéristiques du champ aléatoire (distribution et fonction de corrélation) ainsi que de la méthode de seuillage, lui conférant un aspect prédictif (en termes d'espérance).
Ce contrôle vient de la théorie des champs aléatoires \cite{2008:adler:some} qui nous donne la relation suivante:
\begin{equation}
  \mathbb{E}\{\mathcal{L}_j(\mathcal{E}_\text{s})\} = \sum_{i=0}^{N-j}
  \left(\begin{array}{c}
      i+j\\i
    \end{array}\right)
    \frac{\omega_{i+j}}{\omega_i\omega_j}\left(\frac{\lambda_2}{2\pi}\right)^{i/2}
    \mathcal{L}_{i+j}(M)\mathcal{M}_i^\gamma(\mathcal{H}_\text{s})
    \label{eq:adler}
\end{equation}
où, pour ne parler que des termes les plus importants, $\mathcal{L}_j(\mathcal{E}_\text{s})$ sont des fonctionelles proportionnelles aux mesures géométriques et topologiques de la morphologie ($i=3$: volume, $i=2$: surface, $i=1$: rayon de courbure moyen et $i=0$: caractéristique d'Euler), $\lambda_2$ est le second moment spectral qui contient les informations de la fonction de corrélation du champ et $\mathcal{M}_i^\gamma(\mathcal{H}_\text{s})$ est une fonctionelle de Minkowski qui dépend de la méthode de seuillage $\mathcal{H}_\text{s}$ associé à la mesure de la distribution du champ $\gamma$.

\par L'objectif du modèle est de générer une série de morphologies dont on impose les caractéristiques géométriques et topologiques (partie de gauche de l'équation).
Les paramètres du champ aléatoire correspondant (qui interviennent à droite de l'équation) doivent donc être déterminés en inversant la relation ci-dessus.
La difficulté principale vient du fait qu'un des paramètres du champ est une fonction (la fonction de covariance).
Sans rentrer dans les détails de cette relation fortement non linéaire, il n'est pas encore clair dans la littérature s'il existe nécessairement une solution à ce problème et le cas échéant, si cette solution est unique.
Plus concrètement, on n'arrive pas à trouver les caractéristiques du champ qui permettraient d'obtenir une morphologie semblable à des agrégats, c'est à dire de grandes fractions volumiques tout en conservant une topologie déconnectée.

\vs\par Depuis ma thèse, l'équation \eqref{eq:adler} et ses paramètres ainsi que les interprétations géométriques des fonctionnelles $\mathcal{L}_i$ sont plus clairement définis dans le cadre qui nous intéresse.
Mon avancement est confronté à des difficultés mathématiques qui m'ont poussé à chercher des collaborations avec des mathématiciens (séminaire invité dans l'équipe Probabilité de l'institut Fourier à Grenoble \cite{2021:roubin:excursion}).


% \subsubsection{Liens avec la percolation}
% \begin{draft}
%   \par \lipsum[1-2]
% \end{draft}

