\section{Analyses à l'échelle nanoscopique}
\begin{sidenotes}
    \par Ce projet a partiellement été traité (début de la section \ref{etape:1}) lors de la thèse de Mahban Hosseini \cite{2015:hosseini:numerical}.
\end{sidenotes}

\subsection{Défis et approches}
\par Le projet se penche principalement sur des problématiques scientifiques, actuellement vues comme des verrous scientifiques associés à la modélisation et la compréhension du comportement hydromécanique des matériaux cimentaires.
La voie de recherche, dont le cadre est l'analyse des échelles fines de ces matériaux, se traduit sous la forme de deux verrous scientifiques, le premier (\ref{etape:1}) à court et moyen terme, le second (\ref{etape:2}) à plus longue échéance.

\par La détermination des couplages hydromécaniques au sein des matériaux cimentaires revêt une grande importance dans une optique de prédiction de la durabilité d'un ouvrage.
Ainsi la connaissance des \emph{isothermes de sorption/désorption} est à la base des prédictions de variations dimensionnelles au cours du temps et des recommandations associées.
Le projet cherche à contribuer à la problématique du temps d'acquisition excessivement long (plusieurs années pour un seul cycle de séchage/humidification) de ces données incontournables en apportant une modélisation numérique des phénomènes.
Le premier défi est donc de considérer les matériaux à matrice cimentaire comme des \emph{matériaux nanoporeux}, à la structure morphologique très complexe, et d'extraire de cette morphologie les informations pertinentes quant aux isothermes de sorption/désorption.
\par Le deuxième enjeu scientifique est, par la suite, une analyse mécanique qui doit permettre d'en déduire le retrait de dessiccation associée.
La portée est alors plus générale car il s'agit de renforcer la connaissance des liens existant entre les échelles fines et l'échelle macroscopique.
Le deuxième défi réside donc dans l'\emph{extraction de l'information} des échelles fines et dans l'identification, parmi la grande quantité d'information disponible (géométrique et comportementale), des éléments qui prédominent sur le comportement macroscopique.
L'approche proposée est de contribuer à ce lien par des modèles numériques afin d'avoir une \emph{démarche explicative}.

\vs\par Ces deux axes de recherche sont abordés avec des outils de modélisation numérique morphologique et EF possédant une souche commune \cite{2015:roubin:multi-scale,2015:roubin:meso-scale}, permettant ainsi une certaine unité de l'environnement de travail.

\subsection{Modélisation des isothermes de sorption-désorption}
\label{etape:1}
\par La problématique de cette première étape constitue un verrou majeur dans le domaine des matériaux cimentaires : il s'agit de la détermination des isothermes de sorption/désorption.
En associant la teneur en eau d'une pâte de ciment, d'un mortier ou d'un béton à l'humidité relative de son environnement (à l'équilibre) au cours de plusieurs cycles de séchage (phase de désorption) et d'humidification (phase de sorption), ces courbes contiennent une information très riche liée à l'extrême complexité de l'espace poral de ces matériaux.
\par Du point de vue expérimental, le temps nécessaire à une mesure requiert un état hydrique stable du spécimen \cite{2013:ait-mokhtar:experimental}, ce qui mène fréquemment à des manipulations de plusieurs années pour acquérir plusieurs cycles.
Par ailleurs la mesure associée des variations dimensionnelles (retrait de dessiccation lors des phases de désorption) constitue une donnée majeure dans la caractérisation hydromécanique de ces matériaux mais les mécanismes physiques mis en jeu sont soumis à un intense débat depuis quatre décennies.
\par Concernant le séchage tout d'abord,
\begin{itemize}
\item aux hautes humidités relatives et en supposant un état initial saturé, la plupart des auteurs supposent que la phase fluide est continue \cite{2005:benboudjema:interaction}.
Un écoulement capillaire est alors engendré et régi par la loi de Darcy en régime transitoire.
\item aux humidités relatives intermédiaires et à mesure que celle-ci décroît, la continuité de la phase liquide n'est plus assurée, seuls les pores de faibles diamètres possèdent encore de l'eau à l'état condensé.
L'eau est donc présente sous la forme liquide et gazeuse au sein du réseau poreux.
On est donc en présence de processus d'évaporation/condensation qui assurent le transport diffusif de l'eau vers l'extérieur.
\item aux faibles humidités relatives, seule l'eau adsorbée à la surface des pores et la vapeur d'eau sont encore présentes.
On suppose généralement \cite{2005:benboudjema:interaction} que des processus de diffusions surfacique et gazeuse sont à l'origine des transferts et que, si l'humidité relative est encore diminuée, seule la diffusion gazeuse persiste.
\end{itemize}

\par L'objectif général de ce premier axe du projet est de proposer une approche numérique capable de représenter ces phénomènes de sorption et de désorption, de prédire le comportement hystérétique observé lors des cycles ainsi que d'évaluer les variations dimensionnelles associées.
Au vu de l'importance des enjeux, plusieurs travaux se sont déjà fixé les mêmes objectifs par le passé.
Parmi eux on peut notamment citer \cite{2011:ranaivomanana:toward,2006:bary:a,2003:hilpert:calibration,2008:ahrenholz:prediction}.
Ces études ont toutes en commun de modéliser le réseau poreux sous la forme d'hypothèses très simplificatrices sur le plan géométrique.
Or, nous pensons que, en régime permanent, la complexité des isothermes de sorption/désorption est en grande partie portée par la complexité de cet espace poral.
La négliger ne peut donc pas, à notre sens, permettre une bonne stratégie de modélisation.

\vs\par L'approche adoptée ici est de mettre en oeuvre le modèle morphologique développé précédemment et basé sur les excursions de champs Aléatoires \cite{2015:roubin:meso-scale} pour modéliser l'espace poral d'une pâte de ciment.
Plusieurs essais ont déjà été menés à ce sujet et ont montré la faisabilité de la méthode \cite{2015:hosseini:numerical}.
Le point de départ est ici les données issues de la porosimétrie par intrusion de mercure (donnant la distribution de tailles de pores).
Ce modèle morphologique permet une génération numérique à faible coût d'une grande quantité de réalisations de morphologies de différents types (milieu poreux, matrice/inclusion) dont les caractéristiques géométriques et topologiques globales sont analytiquement contrôlées.
Les nouveautés apportées dans ce projet sont la modélisation d'un milieu à topologie mixte /- les pores peuvent former un réseau fortement percolé alors que les grains de ciment anhydres ou les grains de sable forment plus un ensemble d'inclusions déconnectées /- et la caractérisation souhaitée est beaucoup plus contrainte.
En effet on cherche ici à utiliser toutes les capacités du modèle en contrôlant toutes les données géométriques et topologiques des différentes phases (volumes, surfaces, rayons de courbure, caractéristique d'Euler).
Afin de résoudre les problèmes liés à ces nouveautés, l'adaptation du cadre (Gaussien) classique à des cas plus complexes est donc nécessaire.

\par Dans un second temps, le traitement des images numériques tridimensionnelles générées par un modèle morphologique, typiquement des domaines de \SI{5}{\micro\metre} discrétisé selon des grilles régulières de $800^3$ voxels, soit une résolution d'environ \SI{6}{\nano\metre} se fait par le biais de la morphologie mathématique \cite{1967:matheron:elements} de façon à extraire les informations géométriques pertinentes associées à chaque état d'équilibre hydrique pour différentes humidités relatives : filtrage en tailles de pore par des ouvertures morphologiques associées à la loi de Kelvin-Laplace ; calcul des volumes vidangés lors du séchage par reconstructions géodésiques et recherche de connectivité dans le réseau, etc.
Cette analyse doit notamment permettre de retrouver le comportement complexe, notamment hystérétique, caractéristique des milieux nanoporeux.

\vs\par Le principal verrou de cette première étape du projet est la représentation fine de la morphologie du réseau poreux.
Nous pensons que c'est en représentant cette complexité morphologique que nous parviendrons à reproduire la réponse hydromécanique ``macroscopique'' d'un matériau cimentaire, elle-même très complexe au vu des observations de laboratoire.
\par La grande majorité des modèles morphologiques reposent sur l'assemblage de figures géométriques simples (sphères, ellipsoïdes, \textit{etc}).
Ces modèles comportent de nombreux inconvénients liés à la relation linéaire entre le nombre de paramètres nécessaires à définir une morphologie et son nombre de composants.
Cela entraîne, entre autres, des générations de morphologies aléatoires très coûteuses.
Bien que les excursions permettent de résoudre ce problème, les liens entre les morphologies et les paramètres du modèle restent encore largement inexplorés.
Je souhaite, dans le cadre de ce projet, apporter des \emph{réponses théoriques} à ce problème, permettant ainsi d'apporter une plus grande flexibilité au modèle.

\subsection{Modélisation des variations dimensionnelles associées}
\label{etape:2}
\par En ce qui concerne les variations dimensionnelles, peu de consensus est établi.
Trois mécanismes majeurs sont généralement retenus, lié à la pression capillaire \cite{1982:buil:le}, aux tensions superficielles \cite{1995:hua:analyses} et aux pressions de disjonction \cite{2006:kovler:overview}, respectivement.

\par Le premier mécanisme implique une mise en contrainte de la phase liquide au sein du matériau, engendrée par la pression capillaire, proportionnelle à la tension superficielle du liquide et inversement proportionnelle au rayon de courbure du ménisque.
Une évaporation d'eau aboutit ainsi à une diminution de ce rayon de courbure et à une dépression capillaire créée dans le liquide, provoquant une contraction du matériau.
Ce phénomène est couramment modélisé par les lois dites de Kelvin et de Laplace, qui lient la valeur de cette dépression à l'humidité relative et au rayon de pore capillaire.
Ce mécanisme est considéré comme actif lorsque l'eau capillaire est continue, usuellement pour des humidités relatives au-delà de \SI{40}{\percent}(\cite{1968:powers:mechanism,1970:feldman:a,1988:rossi:a}).
Cette dernière valeur fait néanmoins l'objet d'un débat renouvelé depuis quelques années \cite{2009:wittmann:shrinkage}.

\par Le deuxième mécanisme repose sur les tensions superficielles (ou énergies de surface), qui peuvent varier avec l'adsorption de gaz ou de vapeur sur les gels d'hydrates, considérés comme un système colloïdal.
L'augmentation de ces énergies à mesure que de la vapeur d'eau est désorbée conduit à nouveau à une contraction du matériau, expliquant les retraits observés.
Ce phénomène n'intervient que si la quantité d'eau adsorbée varie, et est donc actif pour des humidités inférieures à \SI{40}{\percent} \cite{1968:wittmann:surface}.
Ce deuxième point, longtemps minoré et donc écarté par les physico-chimistes des matériaux cimentaires, est de nouveau au centre de l'attention.
Quantifier sa part dans le retrait global est aujourd'hui une problématique centrale.

\par Le troisième mécanisme est quant à lui lié aux pressions de disjonction.
L'intrusion d'eau dans les zones interfeuillets induit des forces répulsives entre les feuillets d'hydrates, ainsi qu'une diminution des forces attractives de Van der Waals entre les surfaces solides.
La superposition de ces deux effets est appelée pression de disjonction.
Elle conduit à des variations dimensionnelles \cite{1968:powers:mechanism,1998:visser:extensile}.
La validité de cette pression comme origine des retraits et gonflements observés lors des processus de désaturation/resaturation est toutefois encore controversée, tant dans ses fondements que concernant les gammes d'humidités relatives où elle pourrait être prise en compte.

\par L'enjeu de cette étape est donc de procéder à des analyses mécaniques, linéaires puis non linéaires, sur ces mêmes domaines en considérant les trois sources potentielles de ``pressions'' internes susceptibles de conduire à des variations dimensionnelles et décrites précédemment.
L'objectif est ici de disposer d'un outil numérique suffisamment fin de manière à pouvoir évaluer le rôle de chacun des mécanismes suivant la plage d'humidités relatives considérée.

\subsection{Synthèse et ouverture}
% \par La caractérisation morphologique étant basée sur des phénomènes aléatoires, des outils de calculs statistiques (espérance, variance) sont à mettre en place afin de pouvoir déterminer la représentativité des volumes étudiés (VER).
% Une étude statistique, basée sur la technique dite de Monte-Carlo, nécessite un nombre important de réalisations de morphologies (fournies par le modèle d'excursion développé lors de l'étape précédente), et donc un nombre important de simulations.
% La puissance couplée d'une génération de champs aléatoires basée sur une décomposition orthogonale des variables d'espace et des variables stochastiques d'un côté et de l'utilisation de maillages non adaptés de l'autre devrait permettre d'effectuer la quantité de calculs nécessaire en des temps raisonnables.

\vs\par L'axe de recherche identifié dans ce projet est relativement large et ambitieux en fixant l'échelle d'observation à une précision de quelques nanomètres.
La richesse de la modélisation morphologique ainsi que le sens physique des résultats mécaniques (\eg l'obtention d'un faciès réel de fissuration) offrent un cadre privilégié de rapprochement entre simulations et mesures en apportant une complémentarité numérique aux démarches expérimentales (\eg la quantification de la perméabilité).
Cependant une ouverture intéressante pour la modélisation numérique des isothermes de sorption/désorption serait de prendre en compte, en plus des effets de la capillarité, les phénomènes diffusifs.
Bien qu'étant un effet de second ordre, cela permettrait de prendre en compte un transfert entre des pores de taille plus petite que ce que la physique de l'équation de Kelvin-Laplace considère.
Les outils morpho-mathématiques utilisés pour la capillarité permettraient également de traiter ces nouveaux phénomènes d'un point de vue géométrique.

\par Finalement, l'esprit même de l'approche proposée, dans son ensemble, est de contribuer au lien entre les échelles par des modélisations numériques en adoptant une \emph{démarche explicative}.
Les caractéristiques des outils (excursion de Champs Aléatoires, morpho-mathématiques et MEF enrichis) utilisés permettent une flexibilité appréciable quant à l'identification des micromécanismes (géométriques et mécaniques).
En ce qui concerne les aspects morphologiques, l'utilisation des excursions de champs aléatoires permet de jouer sur la présence ou non de certaines phases ou sur la valeur des différentes caractéristiques géométriques ou topologiques (quantification de la percolation du réseau poreux, quantité de pores fermés, présence de grains, \etc).
Pour ce qui est de la modélisation de la fissuration, le cadre EF à double enrichissement cinématique confère une flexibilité spécialement appréciable quant aux problématiques des interfaces.

\vs\par L'ambition de l'approche proposée réside dans le balayage important des échelles.
Le principal verrou scientifique est donc la quantité importante d'information à traiter, qui donne de la cohérence à l'utilisation de méthodes numériques locales telles que les E-FEM ainsi que le développement de méthodes de décomposition de domaines proposé dans la section précédente.
Cependant d'autres méthodes devront probablement être envisagées pour certains aspects, comme la dynamique moléculaire.
