% ~
% \vfill
%
% \begin{sidenotes}
%   \par Dans ce chapitre introductif je présente dans un premier temps mon parcours académique.
%   Puis, dans un second temps je décris mes thématiques de recherche afin de contextualiser les deux chapitres suivants.
% \end{sidenotes}
%
% \vfill
% ~
\newpage

\section{Parcours académique}

\subsection{Recherche}

\par J'ai effectué ma thèse intitulée ``Meso-scale FE and morphological modeling of heterogeneous media : applications to cementitious materials''
entre 2010 et 2013 au LMT-Cachan sous la direction de Jean-Baptiste Colliat.
Les deux aspects principaux (développement de méthodes EF enrichis et modélisation de structures aléatoires) m'ont permis de développer des compétences d'une part en programmation de méthodes numériques avancées et d'autre part dans un champ des mathématiques lié à la géométrie et la topologie de structures aléatoires.
Ces deux compétences façonnent encore aujourd'hui la manière dont je poursuis mes travaux de recherche.

\par Suite à ma thèse j'ai effectué deux années de postdoc sous la direction de Xavier Oliver à l'UPC de Barcelone en Espagne.
J'ai travaillé sur les mêmes méthodes d'enrichissement d'EF que durant ma thèse mais dans un cadre différent, mêlant à la fois une méthode multiéchelles intégrée (FE$^2$) et une méthode de réduction de modèle.
Ces changements de paradigmes ont enrichi ma connaissance du domaine et ont permis de prendre du recul sur les domaines d'utilisation de certaines méthodes.

\par J'ai ensuite été recruté en septembre 2015 à l'Université Joseph Fourier (actuelle Université Grenoble Alpes) en tant que maître de conférences rattaché au laboratoire 3SR.
C'est en arrivant au laboratoire que j'ai commencé à mettre en place mon projet d'intégration qui consistait à approfondir les liens entre les méthodes numériques que j'avais développées en thèse et l'imagerie 3D \insitu.
Dans ce cadre je suis un des développeurs principaux du logiciel libre {\tt SPAM: Software for Practical Analysis of Material} \cite{2020:stamati:spam:}.
J'ai obtenu deux bourses de thèse de l'école doctorale (Olga Stamati 2016 - 2020) et Rida Grid (2021 - 2024) ainsi qu'un financement de l'université de $40\,000$ euros.

\vs\par Je suis membre élu du conseil scientifique du laboratoire ainsi que responsable de l'animation scientifique (organisation des séminaires), référent HAL de la structure 3SR, référent avec l'UAR de calcul intensif de l'UGA \href{https://gricad.univ-grenoble-alpes.fr/}{Gricad} ainsi que webmaster du \href{https://3sr.univ-grenoble-alpes.fr/}{site internet du laboratoire}.

\subsection{Enseignements}
\par Je suis rattaché à l'IUT1 au département de Génie-Civil et Construction Durable où j'enseigne principalement la géotechnique et les mathématiques pour l'ingénieur.
Je suis également responsable depuis 2021 d'un cours d'introduction à la mécanique des matériaux du Génie-Civil.
En plus de ces enseignements je vais dorénavant intervenir dans d'autres composantes comme le master 2 international Geomechanics, civil engineering and risks où je donnerai un cours sur les liens entre l'imagerie et la simulation ainsi qu’au département Réseaux et télécommunication de l'IUT où je donnerai un cours d'informatique sur la gestion de code.

\par J'ai été directeur des études des deux promotions de septembre 2016 à juillet 2021, responsabilité qui consistait à superviser l'ensemble des étudiants.

\section{Domaines de recherche}

\par Le vieillissement des structures de Génie civil entraîne d'importants questionnements économiques et écologiques quant au choix entre leur maintenance ou leur remplacement. C'est dans ce contexte que se justifie l'approfondissement des connaissances sur le comportement des matériaux cimentaires, sur leurs mécanismes de dégradation à court et long terme ainsi que sur leur comportement au sein d'une structure.
Ce domaine scientifique requiert des compétences interdisciplinaires : mécanique des milieux continus non linéaires, comportement des matériaux cimentaires, transferts de masses dans les milieux poreux, calcul numérique à haute performance.

\par Il est aujourd'hui clairement établi que les mécanismes de dégradation des structures en béton observés à l'échelle macroscopique trouvent leurs origines dans un ensemble de phénomènes physiques et chimiques qui s'opèrent à une échelle plus fine. Leurs conséquences à l'échelle macroscopique sont importantes et ont une influence notable sur la durabilité des structures et donc leur durée de vie.
Ce constat est la pierre angulaire de ce projet qui a pour but de renforcer les liens entre ces échelles.

\vs\par Mes travaux de recherche se situent en amont de la recherche sur le comportement des matériaux cimentaires et améliorent la compréhension des mécanismes aux échelles fines, dans une optique de meilleure prédiction du vieillissement et de la durabilité des structures.
L'objectif principal est de renforcer la connaissance sur les liens entre les caractéristiques mésoscopiques du matériau et leur comportement macroscopique en adoptant une démarche explicative à l'échelle fine.

\subsection{L'échelle mésoscopique}

\par La modélisation du comportement mécanique des matériaux cimentaires à l'échelle mésoscopique fait face à deux problématiques majeures.
La méthode choisie doit être capable de représenter l'aspect hétérogène du matériau à cette échelle ainsi que son comportement quasi fragile (non linéaire).
Une solution que je développe depuis mes travaux de thèse consiste à enrichir la cinématique d'éléments finis classiques en utilisant la méthode E-FEM (Embedded Finite Element Method) \cite{2015:roubin:multi-scale} qui fournit un cadre variationnel permettant de répondre aux deux problématiques.
La formulation du problème possède un aspect fortement local ce qui rend la méthode particulièrement adaptée à des simulations de grande envergure où les hétérogénéités tout comme les faciès de fissuration peuvent avoir des morphologies complexes.
C'est ce dernier point qui justifie son utilisation dans le cadre de simulations mésoscopiques au détriment de méthodes plus globales comme les X-FEM \cite{2016:moradi:computational}.

\par Afin de représenter les hétérogénéités lors d'une simulation Éléments Finis il est possible de créer un maillage adapté à la structure du matériau \cite{2015:you:adaptive}.
Une représentation arbitrairement fine de la géométrie est obtenue au prix de calculs coûteux.
L'utilisation de maillages non adaptés (\ie certains éléments peuvent avoir des n\oe uds dans différentes phases) couplés avec un enrichissement cinématique permet de contourner ces problèmes en modélisant la discontinuité des paramètres matériaux par un saut dans le champ de déformation, offrant par là même un moyen naturel de représenter l'interface comme une variété bidimensionnelle.
Dans le cadre de la thèse d'\ul{Alejandro Ortega Laborin}, la formulation initialement proposée par \cite{2004:markovic:modelisation} a fait l'objet d'une correction, la rendant plus cohérente d'un point de vue variationnel \cite{2021:ortegalaborin:general}.

\par Les mécanismes d'endommagement du matériau à l'échelle mésoscopique prennent en compte l'initiation ainsi que la propagation de microfissures dans chaque phase ainsi que la décohésion entre celles-ci.
Contrairement aux méthodes classiques d'endommagement, ou plus récemment de champs de phase \cite{2019:ulloa:phase-field}, où la détérioration du matériau est représentée par un scalaire à l'échelle de l'élément, un enrichissement cinématique est ici introduit afin de modéliser la fissure au moyen d'un saut dans le champ de déplacement.
Cela permet de résoudre certains problèmes classiques de façon naturelle comme la dépendance au maillage (ici l'énergie est dissipée sur une surface) ou la détermination de l'ouverture de fissures (ici directement représentée par la discontinuité cinématique).

\par Ce double enrichissement cinématique, initialement développé avec des éléments de type barre \cite{2010:benkemoun:failure} a été étendu à une cinématique 3D complète durant ma thèse en restant dans un cadre phénoménologique simple (initiation des microfissures en traction et ouverture en mode I uniquement) et suivant des sollicitations mécaniques restreintes (traction/compression simple) \cite{2015:roubin:multi-scale}.
Dans le cadre de la thèse de \ul{Yue Sun}, la phénoménologie a été adaptée à des modes de rupture locaux en cisaillement ainsi qu'à de la fermeture de fissure, permettant d'appliquer la méthode à des sollicitations cycliques \cite{2021:sun:fe,2021:sun:strong}.
Dans le cadre de la thèse d'\ul{Alejandro Ortega Laborin}, une modification de la formulation la rendant plus cohérente d'un point de vue variationnel a permis de résoudre des problèmes inhérents à la méthode \cite{2001:wells:discontinuous}. De plus, la généralisation du modèle phénoménologique (critère de rupture de type Rankine / Mohr Coulomb et ouverture de fissure en mode mixte) \cite{2021:ortegalaborin:general,2021:ortegalaborin:an,2021:ortegalaborin:general} a rendu plus pertinent l'utilisation de la méthode à des essais confinés.

\subsection{Rôles des hétérogénéités sur les phénomènes de fissuration}

\par Afin de pouvoir utiliser le mésomodèle dans une démarche explicative il est primordial de vérifier sa capacité à prédire le comportement des matériaux cimentaires, que ce soit du point de vue macroscopique (\eg résistance d'un échantillon) ou microscopique (\eg faciès de fissuration, champ de déplacement \dots).

\par C'est dans ce contexte que s'inscrit la thèse d'\ul{Olga Stamati} durant laquelle plusieurs campagnes expérimentales d'essais \textit{in situ} sous tomographie à rayon X ont été effectuées sur des échantillons de microbéton sous des sollicitations de traction simple, compression simple et compressions confinées.
La constitution des matériaux cimentaires rend le traitement des images ainsi que la corrélation entre elles délicat.
Malgré ces problématiques, il a été possible de récupérer plusieurs types de mesures liées à la mésostructure telles que la morphologie des phases (agrégats et macroporosité) \cite{2018:stamati:phase}, les champs de déplacement, l'initiation des fissures ainsi que les faciès de fissuration finaux \cite{2018:stamati:phase,2018:stamati:tensile}.
Ces résultats expérimentaux ont permis de mettre en relief l'impact des hétérogénéités (et notamment de la macroporosité) sur les phénomènes d'initiation et de propagation des fissures \cite{2018:stamati:tensile,2021:stamati:fracturing}.

\par Les morphologies issues de ces campagnes expérimentales ont également été utilisées de manière explicite avec le mésomodèle.
Les paramètres mécaniques du modèle ont été calibrés sur des essais en tractions simples afin de tester ses capacités prédictives en compression simple et compressions confinées.

\begin{figure}[!ht]
	\centering
	\begin{subfigure}[c]{0.5\textwidth}
		\centering
		\includegraphics[width=\textwidth]{media/intro/exp-num}
		\caption{Réponses macroscopiques pour différents confinements (pointillés: expérimental, plein: numérique)}
		\label{fig:exp-num-macro}
	\end{subfigure}
	\hfill
	\begin{subfigure}[c]{0.44\textwidth}
		\centering
		\includegraphics[width=\textwidth]{media/intro/exp-num-cracks}
		\caption{Faciès de fissuration en compression confinée à 5MPa (gauche: expérimental, droite: numérique)}
		\label{fig:exp-num-cracks}
	\end{subfigure}
	\caption{Issu de \cite{2020:stamati:impact}: Comparaison expérimentale et numérique.}
	\label{fig:exp-num}
\end{figure}

\par Les comparaisons entre les grandeurs macroscopiques (fig.(a)) ainsi que les champs (fig.(b)) issus des mesures expérimentales et des simulations numériques ont permis de déterminer le domaine de validité d'une phénoménologie simple (Rankin / Mode I) et de mettre en relief l'importance du rôle des hétérogénéités sur le comportement du matériau \cite{2021:stamati:combination}.
Les limitations du modèle observées pour des confinements élevés justifient les développements de lois de comportements plus complexes effectués dans le cadre de la thèse d'\ul{Alejandro Ortega Laborin} (Mohr Coulomb / Mode mixte).


\par Toujours dans le cadre de la thèse d'\ul{Olga Stamati}, des études préliminaires sur l'impact de la forme des hétérogénéités ont également été menées, révélant l'importance de la représentation explicite de l'aspect anguleux des agrégats, liant ainsi les concentrations de contraintes dues à ces derniers au faciès de fissuration final.
Les limitations dues à la reproductibilité des simulations à morphologie réelle (issues de la tomographie) d'une part et celles dues aux géométries idéalisées des modèles morphologiques simples (\eg empilement de sphères) d'autre part justifient l'utilisation d'un modèle morphologique basé sur des excursions de champs aléatoires \cite{2015:roubin:meso-scale} pour une étude plus approfondie de ces phénomènes.

\subsection{Ouverture vers des simulations à l'échelle de la structure}

\par Mes travaux de postdoc se sont portés sur des méthodes multiéchelles intégrées (de type FE$^2$) afin de pouvoir simuler des structures où les lois de comportements se substituent à des calculs mésoscopiques.
La fissuration à l'échelle macroscopique était également basée sur la E-FEM. Un problème secondaire était résolu en parallèle de l'équilibre macroscopique afin d'assurer la continuité du chemin de fissure \cite{2015:oliver:continuum}.
Une optimisation des calculs mésoscopique était également effectuée en utilisant des méthodes de réductions de modèles adaptés aux volumes élémentaires représentatifs utilisés \cite{2017:oliver:reduced}.

\par Bien que cette méthode soit performante pour des mésostructures simples, il me semble préférable de favoriser d'autres approches afin de pouvoir mieux prendre en compte la complexité de la mésostructure.
C'est dans ce contexte que s'inscrit un des deux projets que je présente par la suite.

\section{Organisation du mémoire}
\par Suite à cette introduction, le manuscrit s'articule en trois chapitres.

\begin{description}
 	\item[Activités de recherche] Le premier chapitre présente en détail mes activités de recherche depuis la fin de ma thèse.
	 Il est articulé autour des résultats principaux des thèses que j'ai encadrées.
	 Il se termine par des réflexions quant aux perspectives possibles de développement des méthodes présentées.
	\item[Projets de recherche] Le deuxième chapitre présente deux projets de recherche que je vais mettre en place qui tendent à pousser l'analyse à l'échelle de la structure et à l'échelle des réseaux nanoporeux.
	Contrairement aux réflexions de la section précédente, ces projets sont plus conséquents dans le sens où, même si basés sur le même socle de méthodes et de compétences que mes travaux précédents, ils nécessitent la mise en place de nouveaux outils et font appel à une culture et une littérature différentes.
	\item[Curriculum Vitae] Le troisième et dernier chapitre présente en détail mon parcours professionnel, mes activités de recherche, mes activités pédagogiques ainsi que mes responsabilités administratives. Puis je présente les trois thèses soutenues que j'ai encadrées et finalement je liste l'ensemble des publications et conférences auquelles j'ai collaboré.
	\end{description}