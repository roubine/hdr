\section{Mesures expérimentales\label{sec:tomo}}

\par Dans cette section sont principalement décrits les travaux de doctorat d'Olga Stamati \cite{2020:stamati:impact} qui portent sur l'effet des hétérogénéités du béton à l'échelle mésoscopique (granulats et porosités d'air occlus) sur le comportement mécanique à l'échelle macroscopique en effectuant des mesures de champs 3D sur des essais \insitu à l'aide de techniques de tomographie à rayons X.
Puis, basés sur des méthodes de corrélation d'image, les champs de déplacements et de déformations expérimentaux sont comparés à des simulations numériques du modèle mésoscopique présenté dans la section précédente.
Une telle approche appliquée aux bétons nécessite des méthodes expérimentales et numériques avancées, ce qui explique le nombre de publications encore très faible sur le sujet et se limitant à des sollicitations simples \cite{2016:nguyen:initiation,2017:yang:in-situ}.
En effectuant des essais triaxiaux, Olga Stamati observe directement le comportement de la microstructure de bétons sous confinement pour la première fois.


\par Mes travaux effectués en collaboration avec Edward And\`o (Ingénieur de Recherche Tomographie au laboratoire 3SR) qui mettent en valeur l'utilisation combinée de tomographie à rayons X et à neutrons \cite{2019:roubin:the} sont également présentés.

\subsection{Mesures de champs sur des microbétons}

\subsubsection{Essais mécaniques \insitu (Olga Stamati)}

\par L'avantage principal qu'apporte la tomographie est la possibilité d'effectuer des essais \insitu durant lesquels la microstructure de l'échantillon est scannée à différents états de chargement.
Cela permet, dans le cas d'essais mécaniques sur des bétons, de révéler des informations importantes sur l'évolution des phénomènes de fissuration.
Cependant, à cause de certaines contraintes inhérentes à la tomographie, les essais \insitu diffèrent des tests classiques effectués en dehors d'un tomographe, notamment en termes de taille d'échantillon.

\vs\par Le choix de la taille de l'échantillon dépend donc de beaucoup de facteurs liés au matériau (taille du plus gros agrégat et de la plus petite hétérogénéité), à la technique d'imagerie (compromis entre la taille du champ de vue (taille de la tomographie) et résolution spatiale) et aux essais mécaniques réalisés (le système de chargement impose certaines dimensions).
Au vu de tous ces facteurs, des échantillons cylindriques d'un diamètre maximal de 13 \si{\milli\metre} pour un élancement de 2 \figref{fig:2020-stamati-tomo-specimen} sont choisis. Ils permettent d'effectuer au laboratoire des essais triaxiaux avec une pression de confinement pouvant aller jusqu'à 15 \si{\mega\pascal}.
Une cellule en aluminium a été spécialement conçue pour ces essais \figref{fig:2020-stamati-tomo-compression}.

\par La composition du béton utilisé conserve les proportions du R30A7 \cite{2008:gabet:triaxial} en coupant la granulométrie à un diamètre maximal de 4 \si{\milli\metre} afin de conserver un compromis entre la variabilité et la représentativité du comportement.
L'appellation \emph{microbéton} vient de ce seuillage de la courbe granulométrique par rapport à un béton ordinaire.

\begin{figure}[!ht]
  \centering
  \begin{subfigure}[t]{0.20\textwidth}
    \centering
    \includegraphics[height=5cm]{media/tomo/2020-stamati-tomo-specimen}
    \caption{Échantillon de microbéton.}
    \label{fig:2020-stamati-tomo-specimen}
  \end{subfigure}
  ~
  \begin{subfigure}[t]{0.45\textwidth}
    \centering
    \includegraphics[height=5cm]{media/tomo/2020-stamati-tomo-compression}
    \caption{Schéma de la cellule conçue pour les essais triaxiaux.}
    \label{fig:2020-stamati-tomo-compression}
  \end{subfigure}
  ~
  \begin{subfigure}[t]{0.30\textwidth}
    \centering
    \includegraphics[height=5cm]{media/tomo/2020-stamati-tomo-setup}
    \caption{Installation dans le tomographe du laboratoire 3SR.}
    \label{fig:2020-stamati-tomo-setup}
  \end{subfigure}
  \caption{Issu de \cite{2021:stamati:fracturing}: Essais triaxiaux \insitu sur des échantillons de microbéton dans le microtomographe du laboratoire 3SR.}
  \label{fig:2020-stamati-tomo}
\end{figure}

\par La campagne expérimentale est faite au laboratoire 3SR qui héberge un tomographe.
Les caractéristiques du tomographe sont classiques pour un tomographe ``de laboratoire'' (détails dans \cite{2020:stamati:impact}) et permettent d'obtenir une résolution correspondant à des pixels de 13 \si{\micro\metre} ce qui permet de distinguer les plus petits pores ($\approx$ 100 \si{\micro\metre}) tout en gardant une fenêtre de vue de 30 \si{\milli\metre} de côté, et ce, malgré la cellule en aluminium nécessaire aux essais confinés \figref{fig:2020-stamati-tomo-setup}.

\par Des essais de tractions (2), de compressions simples (3) et de compressions confinées (à 5, 10 et 15 \si{\mega\pascal}) son effectués.
Pour chaque essai des scans sont effectués au cours de chargement avec une moyenne de 5 scans par essai \figref{fig:2020-stamati-campagn}.
Le comportement macroscopique montre bien que malgré la petite taille des échantillons par rapport aux hétérogénéités, le comportement était bien représentatif d'un béton ordinaire avec une asymétrie classique du comportement en traction/compression et des modes de rupture cohérent avec le chargement.

% \clearpage
\begin{figure}[!ht]
  \centering
  \includegraphics[width=0.80\textwidth]{media/tomo/2020-stamati-campagn}
  \caption{Résumé de la campagne expérimentale \insitu.}
  \label{fig:2020-stamati-campagn}
\end{figure}

\subsubsection{Mesure de champs et fissuration (Olga Stamati)}

\begin{sidenotes}
  \par Au laboratoire 3SR, le code de corrélation d'image utilisé (et développé) par la cellule technique tomographie était basé sur une méthode dite de pixel search où, en effectuant une recherche ``brute force'' dans toutes les directions, des ensembles de pixels étaient retrouvés d'une image à l'autre, générant ainsi un champ de déplacement \cite{2017:tudisco:tomowarp2:}.
  Je suis arrivé au laboratoire au début des réflexions sur le développement de méthodes plus efficaces.
  J'ai donc contribué au lancement d'un nouveau logiciel dédié en partie à la corrélation d'image, ``SPAM: Software for Practical Analysis of Materials'', dont les principaux développeurs sont Olga Stamati, Edward And\`o et moi-même \cite{2020:stamati:spam:}.
\end{sidenotes}

\vs\par La méthode de corrélation volumique d'image (DVC pour Digital Volume Correlation) que nous avons implémentée \spam rentre dans la catégorie des méthodes locales \cite{1981:lucas:an,2012:hild:comparison}.
Une première image de référence $f(\x)$ et une seconde image ``déformée'' (scan d'un autre état de chargement) $g(\x)$ servent à déterminer un opérateur de transformation $\bphi$ qui contient trois translations, trois rotations et trois déformations.
La détermination de cet opérateur se fait en minimisant la fonctionnelle suivante:
\begin{equation}
  \eta^2 = \sum_{\x\in\Omega} \frac{1}{2}\left(f(\x) - g(\bphi\cdot\x)\right)^2
  \label{eq:correlation:min}
\end{equation}
où $\x$ représente chaque point de la zone d'intérêt $\Omega$.
Minimiser cette fonctionnelle revient à faire correspondre à un point matériel à la position $\x$ de l'image de référence $f$, le même point matériel à la position $\x'=\bphi\cdot\x$ de l'image déformée $g$.
La résolution de \eqref{eq:correlation:min} pour l'ensemble de l'image ($\Omega = \text{image}$) s'appelle un recalage et ne confère qu'un seul opérateur (et non un champ) pour l'ensemble de l'échantillon $\bphi_\text{reg}$.
Une linéarisation de \eqref{eq:correlation:min} entraine naturellement une résolution de type Newton-Raphson où la direction de la solution est calculée à l'aide du gradient de l'image.
Il est donc primordial pour le bon fonctionnement de cette méthode de résolution que la texture soit plus importante que le bruit de l'image.


\par Afin d'obtenir un champ de transformations (ce qui inclut un champ de déplacements), les images sont divisées spatialement en plusieurs imagettes $\Omega_\text{e}$ et la résolution de \eqref{eq:correlation:min} pour chacune d'elles.
Le champ de $\bphi$ ainsi obtenu peut donc être utilisé pour extraire un champ de déplacement par exemple.
La discrétisation de ce champ sera donc directement liée au découpage en imagettes, entrainant un compromis inévitable entre la finesse de la discrétisation du champ cinématique obtenu (pour de petites imagettes) et la robustesse des corrélations (plus $\Omega_\text{e}$ est petit, plus \eqref{eq:correlation:min} est difficile à résoudre).

\vs\par Appliquée aux bétons, la corrélation d'images représente plusieurs défis inhérents aux matériaux qui le compose.
Premièrement les déformations engendrées par les essais sont très faibles par rapport à la précision des déplacements que l'on peut mesurer.
Deuxièmement la très faible texture de certaines de ses phases (notamment les agrégats) ne permet pas l'utilisation de méthodes de corrélation d'images conventionnelles.
Malgré cela, les méthodes suivantes ont permis d'obtenir des champs cinématiques satisfaisants.

\begin{description}
  \item[Solution initiale] Les méthodes de Newton-Raphson étant très sensibles aux conditions initiales, un recalage initial est effectué afin de déterminer une déformation globale $\bphi_\text{reg}$.
  Cette étape est généralement robuste, car effectuée sur l'image entière.
  Ensuite $\bphi_\text{reg}$ est reporté sur chaque imagette et sert de condition initiale aux résolutions de \eqref{eq:correlation:min}, augmentant drastiquement la stabilité du schéma.
  \item[Approche multiéchelles (binning)] $\eta$ est généralement régulier et convexe proche de la solution, facilitant ainsi la convergence des schémas itératifs.
  Cependant dans le cas de grandes déformations (grandes par rapport à la taille caractéristique de la texture), la validité du schéma est à remettre en cause.
  Toujours dans l'objectif de définir une solution initiale au plus proche du minimum, il est courant d'effectuer en amont une corrélation sur une image dégradée où des voxels voisins sont moyennés en un seul (binning) \cite{1981:lucas:an,2006:hild:digital,2007:helfen:synchrotron-radiation}.
  La notion de multiéchelles vient ici du fait que cette méthode peut être répétée autant de fois que nécessaire, partant d'une image de la microstructure très dégradée (grande échelle) et allant au fur et à mesure vers des images mieux résolues (échelles fines).
  \item[Corrélations locales et discrètes combinées] L'aspect le plus novateur et qui permet de régler le problème de texture des agrégats et de combiner deux méthodes de corrélations, une pour chaque phase du matériau.
  \begin{itemize}
    \item La méthode de corrélation locale, décrite jusque là, qui permet de corréler la phase de mortier qui possède la texture suffisante à la convergence des schémas de Newton-Raphson \figref{fig:2020-stamati-combined-3}.
    \item La méthode de corrélation discrète sur chaque agrégat.
    Cette méthode développée pour les milieux granulaires \cite{2010:hall:discrete,2012:ando:experimental} se décompose en deux temps.
    Dans un premier temps tous les agrégats sont identifiés (forme et position) \figref{fig:2020-stamati-combined-1}.
    Puis, dans un second temps, un recalage de chacun des agrégats permet de leur attribuer un $\bphi_\text{reg}$.
    C'est le fort gradient conféré par l'identification de la forme des agrégats qui permet la résolution de \eqref{eq:correlation:min} malgré le manque de texture.
  \end{itemize}
  La corrélation discrète est ensuite projetée sur la maille régulière que la corrélation local \figref{fig:2020-stamati-combined-2} afin de combiner les deux champs sur tout l'échantillon.
\end{description}


\begin{figure}[!ht]
  \centering
  \begin{subfigure}[t]{0.23\textwidth}
    \centering
    \includegraphics[height=5cm]{media/tomo/2020-stamati-combined-1}
    \caption{Identification des agrégats et recalage.}
    \label{fig:2020-stamati-combined-1}
  \end{subfigure}
  ~
  \begin{subfigure}[t]{0.23\textwidth}
    \centering
    \includegraphics[height=5cm]{media/tomo/2020-stamati-combined-2}
    \caption{Champ issu de la corrélation discrète.}
    \label{fig:2020-stamati-combined-2}
  \end{subfigure}
  ~
  \begin{subfigure}[t]{0.23\textwidth}
    \centering
    \includegraphics[height=5cm]{media/tomo/2020-stamati-combined-3}
    \caption{Champ issu de la corrélation locale.}
    \label{fig:2020-stamati-combined-3}
  \end{subfigure}
  ~
  \begin{subfigure}[t]{0.23\textwidth}
    \centering
    \includegraphics[height=5cm]{media/tomo/2020-stamati-combined-4}
    \caption{Fusion des deux champs.}
    \label{fig:2020-stamati-combined-4}
  \end{subfigure}
  \caption{Issu de \cite{2021:stamati:fracturing}: Combinaison de corrélation locale sur le mortier et discrète sur les agrégats afin d'obtenir un champ cinématique sur l'ensemble de l'échantillon.}
  \label{fig:2020-stamati-combined}
\end{figure}

\vs\par Au final, cette méthodologie est appliquée à tous les essais \figref{fig:2020-stamati-campagn}.
En raison des déformations très faibles, les corrélations les plus problématiques étaient celles des essais de traction simple.
Comme le montre \figref{fig:2018-stamati-correlation-tension-1} la tendance des déplacements obtenus cohérente à 75\% de la charge.
En calculant le champ de déformation, on observe une zone de forte concentration des déformations \figref{fig:2018-stamati-correlation-tension-2}.
En le superposant au faciès de fissuration macroscopique observé en fin d'essai \figref{fig:2018-stamati-correlation-tension-4} on se rend compte que la localisation des déformations correspond à l'initiation de la fissure.
Ce résultat est d'autant plus intéressant que cette information n'est pas détectable sur l'image seule et que c'est bien la corrélation qui la met en valeur (plus de détails sur la détection des fissures sont donnés dans la section suivante).

\begin{figure}[!ht]
  \centering
  \begin{subfigure}[t]{0.23\textwidth}
    \centering
    \includegraphics[height=5cm]{media/tomo/2020-stamati-correlation-tension-1}
    \caption{Déplacement à 75\% de la charge.}
    \label{fig:2018-stamati-correlation-tension-1}
  \end{subfigure}
  ~
  \begin{subfigure}[t]{0.23\textwidth}
    \centering
    \includegraphics[height=5cm]{media/tomo/2020-stamati-correlation-tension-2}
    \caption{Déformation axiale à 75\% de la charge.}
    \label{fig:2018-stamati-correlation-tension-2}
  \end{subfigure}
  ~
  \begin{subfigure}[t]{0.23\textwidth}
    \centering
    \includegraphics[height=5cm]{media/tomo/2020-stamati-correlation-tension-3}
    \caption{Résidu $\eta$ de corrélation à 75\% de la charge.}
    \label{fig:2018-stamati-correlation-tension-3}
  \end{subfigure}
  ~
  \begin{subfigure}[t]{0.23\textwidth}
    \centering
    \includegraphics[height=5cm]{media/tomo/2020-stamati-correlation-tension-4}
    \caption{Faciès de fissuration après rupture.}
    \label{fig:2018-stamati-correlation-tension-4}
  \end{subfigure}
  \caption{Issu de \cite{2018:stamati:tensile}: Évolution de la fissuration lors d'un test de traction simple.}
  \label{fig:2018-stamati-correlation-tension}
\end{figure}

\vs\par En résumé, la corrélation d'image à partir d'essais \insitu permet d'obtenir l'évolution de champs cinématiques au cours de la charge, nous renseignant sur les modes de ruptures du béton.
Il a été confirmé que les phénomènes de décohésion jouent un rôle dominant lors de l'initiation des fissures et que la macroporosité influence significativement leurs propagations.
Cependant l'aspect aléatoire de ces phénomènes ne permet pas de tirer des conclusions sans analyses statistiques, ce que cette méthodologie d'essais \insitu ne permet pas de faire.
Une discussion est proposée sur ce sujet en conclusion de ce mémoire.
\par D'autres résultats de corrélation seront présentés et comparés à des simulations numériques dans la section suivante.

\subsection{Liens avec les mésomodèles}

\par En plus de l'analyse des mécanismes de fissuration, les essais \insitu offrent la possibilité de faire un lien direct entre résultats expérimentaux et numériques, tous deux possédant: une représentation explicite de la microstructure d'une part, des résultats commensurables comme des champs cinématiques 3D et des faciès de fissuration de l'autre.
Une première étape est donc de récupérer la morphologie ``réelle'' issue de la tomographie, puis, à l'issue de la simulation numérique, les différents champs peuvent être comparés.

\subsubsection{Extraction de la morphologie (Olga Stamati)}

\par Afin de pouvoir utiliser la morphologie issue de la tomographie dans une simulation mésoscopique, il faut réussir à identifier chacune des phases.
Ici il nous faut identifier la macroporosité et les agrégats (le mortier étant le complément des deux) \figref{fig:2020-stamati-morpho-1}.
Ce procédé peut être simplement effectué par une méthode de seuillage des niveaux de gris dans le cas de la macroporosité.
En effet, les niveaux de gris sont assez distincts du mortier et des agrégats pour pouvoir les séparer ainsi \figref{fig:2020-stamati-morpho-2}.

\par Cependant, une méthode de seuillage sur les niveaux de gris n'est plus applicable pour discerner les agrégats du mortier.
En effet, le niveau de gris obtenu sur les tomographies dépendent des matériaux constituant les phases (coefficient d'absorption aux rayons X), et les agrégats et le mortier étant constitués des mêmes matériaux, les deux phases ont, en moyenne, les mêmes niveaux de gris, rendant le seuillage impossible \figref{fig:2020-stamati-morpho-2}.

\par Notre méthodologie pour résoudre ce problème a été l'objet d'une publication \cite{2018:stamati:phase} où nous utilisons la différence de texture observée entre les agrégats et le mortier (déjà mentionnée dans la section précédente sur la corrélation d'image).
Cette différence de texture vient d'une plus grande hétérogénéité du mortier par rapport aux agrégats et peut être utilisée par un gradient spatial \figref{fig:2020-stamati-morpho-3}.
Le seuillage est ensuite fait sur le champ de gradient de niveau de gris permettant ainsi l'identification des agrégats par rapport au mortier.

\begin{figure}[!ht]
  \centering
  \begin{subfigure}[t]{0.32\textwidth}
    \centering
    \includegraphics[height=5cm]{media/tomo/2020-stamati-morpho-1}
    \caption{Niveaux de gris.}
    \label{fig:2020-stamati-morpho-1}
  \end{subfigure}
  ~
  \begin{subfigure}[t]{0.65\textwidth}
    \centering
    \includegraphics[height=5cm]{media/tomo/2020-stamati-morpho-2}
    \caption{Histogramme des niveaux de gris.}
    \label{fig:2020-stamati-morpho-2}
  \end{subfigure}\\
  \begin{subfigure}[t]{0.32\textwidth}
    \centering
    \includegraphics[height=5cm]{media/tomo/2020-stamati-morpho-3}
    \caption{Gradient des niveaux de gris.}
    \label{fig:2020-stamati-morpho-3}
  \end{subfigure}
  ~
  \begin{subfigure}[t]{0.65\textwidth}
    \centering
    \includegraphics[height=5cm]{media/tomo/2020-stamati-morpho-4}
    \caption{Projection de la morphologie sur un maillage EF.}
    \label{fig:2020-stamati-morpho-4}
  \end{subfigure}
  \caption{Issu de \cite{2018:stamati:phase}: Méthode d'identification des phases de tomographies de béton.}
  \label{fig:2020-stamati-morpho}
\end{figure}

\par La combinaison du seuillage des niveaux de gris pour identifier la macroporosité et du seuillage sur le gradient des niveaux de gris pour identifier les agrégats permet d'obtenir une image trinarisée représentant la mésosctructure du béton.
Cette image est ensuite projetée sur un maillage EF régulier afin de déterminer les différentes données géométriques nécessaires au modèle mésoscopique relative aux discontinuités faibles \figref{fig:2020-stamati-morpho-4}.

\subsubsection{Comportement macroscopique (Olga Stamati et Alejandro Ortega)}
\par La première comparaison faite entre les résultats expérimentaux et le mésomodèle concerne les grandeurs macroscopiques.
Les caractéristiques du modèle sont calibrées sur les essais de traction (qui possèdent une très faible variabilité malgré la taille des agrégats) et ne sont pas modifiées pour les essais de compression.
Deux modèles sont utilisés afin de comparer l'impact de la description des mécanismes de rupture sur le comportement:
\begin{description}
  \item[Modèle en mode I] C'est le modèle issu de ma thèse \cite{2015:roubin:multi-scale} qui a été utilisé exclusivement dans la thèse de Olga Stamati afin d'être comparé à sa campagne expérimentale.
  La localisation de la fissuration se fait uniquement selon un critère de Ranking et l'ouverture est en mode I (perpendiculaire à la surface de la fissure).
  \item[Modèle généralisé] C'est le modèle issu de la généralisation de la formulation variationnelle proposé lors de la thèse de Alejandro \cite{2021:ortegalaborin:general} qui a également été testé sur ces essais.
  La localisation se fait selon un critère de Ranking et de Mohr-Coulomb et l'ouverture est une combinaison de mode I et mode II.
\end{description}

Les courbes contraintes/déformation sont tracées \figref{fig:2021-ortega-macro} pour les résultats expérimentaux et les deux modèles.

\begin{figure}[!ht]
  \centering
  \includegraphics[width=0.90\textwidth]{media/comparison/2021-ortega-macro}
  \caption{Issu de \cite{2021:ortegalaborin:phd}: comparaison des comportements macroscopiques entre les résultats expérimentaux (pointillés), modèle en mode I (tirets), modèle généralisé (traits pleins).}
  \label{fig:2021-ortega-macro}
\end{figure}

\par La première conclusion tirée de ses courbes et que, malgré sa simplicité (rupture exclusivement en traction simple) et malgré sa calibration sur des essais de traction, le modèle est capable de reproduire la tendance du comportement en compression simple et confinée.
On observe que la résistance et la ductilité sont cependant un peu faible par rapport aux résultats expérimentaux.

\par L'introduction d'un mode de rupture plus complexe permet, comme attendu, de prédire le comportement de manière plus exacte, bien que pour certains confinements la ductilité soit encore sous-estimée et la résistance surestimée.
Cependant ces essais numériques n'ont pas encore fait l'objet d'une compagne d'identification complète.
En effet maintenant que plus de paramètres matériaux sont disponibles, le principe d'identification en traction simple est à remettre en cause.
Une discussion sur ce sujet est proposée en conclusion.


\subsubsection{Champs cinématiques et faciès de fissuration (Olga Stamati)}
\begin{sidenotes}
\par Pour des raisons de clarté et au vu du manque de maturité des résultats des modèles plus complexes, uniquement les comparaisons avec le modèle en mode I sont présentées dans cette section.
\end{sidenotes}

\vs\par Comme présenté plus haut, les corrélations d'images sur les tomographies issues des essais \insitu ont produit des champs cinématiques 3D au cours du chargement.
Il est donc possible de les comparer directement aux champs cinématiques des essais numériques, comme le montre \figref{fig:2020-stamati-displacements-Tri5} pour l'essai de compression confinée à 5\si{\mega\pascal}.

% \begin{figure}[!ht]
%   \centering
%   \includegraphics[width=0.80\textwidth]{media/comparison/2020-stamati-displacements-Tri5}
%   \label{fig:2020-stamati-displacements-Tri5}
% \end{figure}

\begin{figure}[!ht]
  \centering
  \begin{subfigure}[t]{0.22\textwidth}
    \centering
    \includegraphics[height=5cm]{media/comparison/2020-stamati-displacements-Tri5-1}
    \caption{Expérimental}
    \label{fig:2020-stamati-displacements-Tri5-1}
  \end{subfigure}
  ~
  \begin{subfigure}[t]{0.22\textwidth}
    \centering
    \includegraphics[height=5cm]{media/comparison/2020-stamati-displacements-Tri5-2}
    \caption{Numérique}
    \label{fig:2020-stamati-displacements-Tri5-2}
  \end{subfigure}
  ~
  \begin{subfigure}[t]{0.22\textwidth}
    \centering
    \includegraphics[height=5cm]{media/comparison/2020-stamati-displacements-Tri5-3}
    \caption{Expérimental}
    \label{fig:2020-stamati-displacements-Tri5-3}
  \end{subfigure}
  ~
  \begin{subfigure}[t]{0.22\textwidth}
    \centering
    \includegraphics[height=5cm]{media/comparison/2020-stamati-displacements-Tri5-4}
    \caption{Numérique}
    \label{fig:2020-stamati-displacements-Tri5-4}
  \end{subfigure}
  \caption{Issu de \cite{2020:stamati:impact}: Champs de déplacement expérimentaux et numériques pour la compression triaxiale à 5\si{\mega\pascal} à deux niveaux de chargement: (a) et (b) avant rupture et (c) et (d) après rupture.}
  \label{fig:2020-stamati-displacements-Tri5}
\end{figure}

\par Par principe le champ de déplacement numérique englobe toutes les informations cinématiques.
Cependant ici, les EF possèdent une discontinuité forte (saut dans le champ de déplacement) qui représente donc directement l'aspect discontinu d'une fissure.
Il est donc pertinent d'utiliser cette information afin de comparer les processus de fissuration.
De par l'aspect local des E-FEM, un élément seul affichant une telle discontinuité ne représente donc aucunement une entité représentative à l'échelle macroscopique.
On peut cependant voir émerger du calcul une fissure macroscopique lorsqu'un grand nombre de ces microfissures s'agrègent.
On observe également qu'une macrofissure définie de la sorte possède naturellement des propriétés complexes telles que \emph{la continuité et la ramification (branching) du chemin de fissure}.
D'un point de vue expérimental, la transition entre une localisation continue des déformations et la création d'une discontinuité (microfissure) peut être observée grâce aux champs de déformations issues des déplacements mesurées grâce à la corrélation d'images.
Cependant la distinction entre localisation et discontinuité est non triviale et il est nécessaire d'avoir d'autres moyens afin de discerner la nature des phénomènes observés.
Les trois moyens directs et indirects d'observer une fissure sont:
\begin{description}
  \item[Seuillage des niveaux de gris] Cette méthode est la méthode triviale qui consiste à seuiller l'image et considérer les vides (en dehors des pores identifiés auparavant) comme des fissures.
  Cependant, la taille des fissures est, en début de localisation, souvent plus faible que la résolution de l'image.
  Il est donc pertinent d'utiliser cette méthode uniquement en fin d'essai, après la ruine de l'échantillon \figref{fig:2018-stamati-correlation-tension-4}.
  \item[Localisation des déformations] Cette méthode nécessite donc de la corrélation d'image volumique afin d'obtenir les champs cinématiques nécessaires au calcul des déformations.
  Comme observé \figref{fig:2018-stamati-correlation-tension-2}, elle permet de détecter l'initiation d'une macrofissure.
  Cependant, il n'est pas possible de mesurer directement une discontinuité et donc d'affirmer que la localisation correspond bien à une fissure.
  La superposition du champ de déformation avec le faciès de fissuration finale peut permettre de le confirmer.
  \item[Résidu de corrélation] Le résidu de corrélation $\eta$ issu de la résolution du problème de minimisation \eqref{eq:correlation:min} de la corrélation d'image permet également de renseigner sur l'état de fissuration du matériau \cite{2018:chateau:dvc-based}.
  Ce champ représente la qualité de la corrélation (plus ses valeurs sont grandes, moins la corrélation est bonne) et vu que la corrélation d'image est incapable de représenter des discontinuités cinématiques locales (car $\bphi$ est affine), une fissure apparait de manière nette dans le champ de résidu \figref{fig:2018-stamati-correlation-tension-3}.
  C'est ce procédé qui nous a permis d'augmenter la sensibilité de la détection des fissures.
\end{description}

\begin{figure}[!ht]
  \centering
  \begin{subfigure}[t]{0.45\textwidth}
    \centering
    \includegraphics[height=5cm]{media/comparison/2020-stamati-cracks-T}
    \caption{Essai de traction simple.}
    \label{fig:2020-stamati-cracks-T}
  \end{subfigure}
  ~
  \begin{subfigure}[t]{0.45\textwidth}
    \centering
    \includegraphics[height=5cm]{media/comparison/2020-stamati-cracks-C}
    \caption{Essai de compression simple.}
    \label{fig:2020-stamati-cracks-C}
  \end{subfigure}\\
  \begin{subfigure}[t]{0.45\textwidth}
    \centering
    \includegraphics[height=5cm]{media/comparison/2020-stamati-cracks-Tri5}
    \caption{Essai de compression triaxiale à 5\si{\mega\pascal}}
    \label{fig:2020-stamati-cracks-Tri5}
  \end{subfigure}
  ~
  \begin{subfigure}[t]{0.45\textwidth}
    \centering
    \includegraphics[height=5cm]{media/comparison/2020-stamati-cracks-Tri15}
    \caption{Essai de compression triaxiale à 15\si{\mega\pascal}}
    \label{fig:2020-stamati-cracks-Tri15}
  \end{subfigure}
  \caption{Issu de \cite{2020:stamati:impact}: Comparaison des faciès de fissuration expérimentaux (à gauche) et numériques (à droite). Les faciès de fissuration expérimentaux sont obtenus par seuillage de niveau de gris pour (a) et (b) et par seuillage de résidu $\eta$ pour (c) et (d).}
  \label{fig:2020-stamati-cracks}
\end{figure}

\vs\par En choisissant la méthode la plus adaptée en fonction de l'état de ruine de l'échantillon et de la complexité du faciès de fissuration, la comparaison entre expérience et numérique est montrée \figref{fig:2020-stamati-cracks} pour les différents types de sollicitation.
On observe pour la traction simple que les deux macrofissures suivent le même chemin.
La présence de fissures supplémentaire sur les résultats de simulation numériques peut être expliquée par des aspects numériques et expérimentaux.
Premièrement le modèle numérique présenté ici ne permet pas aux fissures de se refermer lors de la relaxation induite par l'ouverture de la macrofissure et deuxièmement, les déplacements des essais de tractions étant très faibles, il est possible que l'amplitude d'une fissure secondaire soit de l'ordre du bruit de la mesure et donc non détectable expérimentalement.
En compression confinée à 5\si{\mega\pascal} on retrouve dans les deux cas une bande de cisaillement bien que numériquement la cinématique locale n'accorde qu'une localisation et ouverture en mode I.
Enfin, pour l'essai à 15\si{\mega\pascal}, la bande de compaction observée expérimentalement correspond à une forte densité de microfissures lors de la simulation.


\vs\par De manière générale, on observe que les champs de déplacement (et de déformations) ainsi que les faciès de fissuration concordent ``relativement'' bien (des études quantitatives non présentées ici ont été menées dans \cite{2020:stamati:impact}).
Les différences émergent principalement lorsque la localisation est très prononcée (formation d'une ou plusieurs macrofissures).
Elles peuvent être expliquées en partie par la phénoménologie trop simplifiée du modèle en mode I (déjà observé au niveau macroscopique proche de la rupture \figref{fig:2021-ortega-macro}).
D'autres facteurs tels qu'une mauvaise représentation des hétérogénéités dues à des variations de la composition des agrégats sont également observés.
Bien que statistiquement mineurs d'un point de vue morphologique, ces erreurs du modèle numérique peuvent entrainer d'importantes différences d'un point de vue de l'initiation et de la propagation de la fissuration.
De plus, nous avons montré que la forme des hétérogénéités joue un rôle important dans le comportement macroscopique et le développement de la fissuration, rendant les problématiques d'extraction de la morphologie et d'aptitude à la modéliser primordiales.


\subsection{Rayons X et neutrons}

\par Les tomographies à rayons X et à neutrons donnent toutes les deux des informations cruciales sur la microstructure des géomatériaux.
Cependant, la différence de sensibilité produit des contrastes est différente rendant chaque modalité intéressante pour un composant spécifique de la microstructure.
Dans le cas des bétons, la différence principale est que les images issues des neutrons auront un contraste principalement basé sur la présence d'eau alors que les images issues des rayons X auront un contraste principalement basé sur la densité des phases.
Si des images du même échantillon sont produites avec chacune des modalités, il est nécessaire de les recaler l'une par rapport à l'autre afin d'obtenir des champs spatialement commensurables \figref{fig:2019-roubin-xn}.
Basé sur les travaux de \cite{2017:tudisco:an} et en collaboration avec Edward And\`o (laboratoire 3SR) et Stéphane Roux (LMT Cachan), une méthode de recalage d'images possédant différentes modalités a été appliqué à des échantillons de béton \cite{2019:roubin:the} et le logiciel développé à l'occasion \cite{2020:stamati:spam:} est couramment utilisé sur les projets de l'instrument D50 du projet NeXT à l'ILL \cite{2020:tengattini:next-grenoble}.

\begin{figure}[!ht]
  \centering
  \begin{subfigure}[t]{0.45\textwidth}
    \centering
    \includegraphics[width=0.9\textwidth]{media/neutrons/2019-roubin-xn-x}
    \caption{$f_\text{x}(\x)$: champ d'atténuation des rayons X.}
    \label{fig:2019-roubin-xn-x}
  \end{subfigure}
  ~
  \begin{subfigure}[t]{0.45\textwidth}
    \centering
    \includegraphics[width=0.9\textwidth]{media/neutrons/2019-roubin-xn-n}
    \caption{$f_\text{n}(\x)$: champ d'atténuation des neutrons.}
    \label{fig:2019-roubin-xn-n}
  \end{subfigure}
  \caption{Issu de \cite{2019:roubin:the}: Coupe horizontale de tomographie à rayons X et neutrons du même échantillon.}
  \label{fig:2019-roubin-xn}
\end{figure}

\vs\par L'objectif du recalage est le même que pour la corrélation d'image classique où l'on cherche à déterminer un opérateur de transformation $\bphi_\text{reg}$ qui minimise une certaine fonctionnelle $\eta$.
Cependant, si pour la corrélation d'image classique la différence entre les deux images $f$ et $g$ est commensurable (\ie il est pertinent d'évaluer $f-g$), ici la différence entre deux images $f_\text{x}$ (pour les rayons X \figref{fig:2019-roubin-xn-x}) et $f_\text{n}$ (pour les neutrons \figref{fig:2019-roubin-xn-n}) n'a pas de sens dû à leurs différentes modalités.
\par Le problème peut se contourner en travaillant sur un histogramme joint (\figref{fig:2019-roubin-xn-jointhist}) des niveaux de gris des deux modalités: $f_\text{n}$ (en ordonnée) et $f_\text{x}$ (en abscisse).
L'histogramme joint se représente donc en 2D où chaque maximum local correspond à une phase.
Sur la figure \figref{fig:2019-roubin-xn-jointhist}, de haut en bas on reconnait: le mortier, les agrégats et les vides.
L'étalement de ces maxima s'explique par l'aspect hétérogène des phases et les erreurs de mesure d'une part et un recalage non parfait de l'autre.
Ce deuxième point est important et une accentuation de ces maxima peut être une mesure de la qualité du recalage.

\begin{figure}[!ht]
  \centering
  \begin{subfigure}[t]{0.45\textwidth}
    \centering
    \includegraphics[height=5cm]{media/neutrons/2019-roubin-xn-jointhist}
    \caption{Histogramme joint.}
    \label{fig:2019-roubin-xn-jointhist}
  \end{subfigure}
  ~
  \begin{subfigure}[t]{0.45\textwidth}
    \centering
    \includegraphics[height=5cm]{media/neutrons/2019-roubin-xn-jointhist-fitted}
    \caption{Fit de l'histogramme joint.}
    \label{fig:2019-roubin-xn-jointhist-fitted}
  \end{subfigure}\\
  \begin{subfigure}[t]{0.45\textwidth}
    \centering
    \includegraphics[height=5cm]{media/neutrons/2019-roubin-xn-checkerboard}
    \caption{Superposition des deux images recalées.}
    \label{fig:2019-roubin-xn-checkerboard}
  \end{subfigure}
  ~
  \begin{subfigure}[t]{0.45\textwidth}
    \centering
    \includegraphics[height=5cm]{media/neutrons/2019-roubin-xn-segmented}
    \caption{Identification des phases.}
    \label{fig:2019-roubin-xn-segmented}
  \end{subfigure}
  \caption{Issu de \cite{2019:roubin:the}: Différentes étapes du recalage d'images aux modalités différentes.}
\end{figure}


\par L'histogramme est ensuite approximé par une série de gaussiennes (une Gaussienne par phase) afin de conférer à la représentation de cet histogramme un aspect analytique \figref{fig:2019-roubin-xn-jointhist-fitted}.
Ces gaussiennes englobent donc toutes les informations qui lient les deux modalités $f_\text{x}$ et $f_\text{n}$.
On peut donc déterminer une fonctionnelle $\mathcal{L}(f_\text{x}, f_\text{n})$ basé sur ces gaussiennes afin de retomber sur l'équivalent du problème de minimisation classique \eqref{eq:correlation:min}:
\begin{equation}
  \eta^2 = \sum_{\x\in\Omega} \mathcal{L}^2(f_\text{x}(\x), f_\text{n}(\x))
  \label{eq:correlation:min:multi}
\end{equation}
On définit $\mathcal{L}$ par
\begin{equation}
  \mathcal{L}^2(\boldsymbol{\varphi}) =
  -\ln\left(
  \sum_i^\text{phases} \phi_i \exp\left( -\frac{1}{2} (\boldsymbol{\varphi}-\boldsymbol{\mu}_i)^t \boldsymbol{C}_i^{-1} (\boldsymbol{\varphi}-\boldsymbol{\mu}_i) \right)
  \right)
\end{equation}
avec $\boldsymbol{\varphi} = [f_\text{x}, f_\text{n}]^t$ et $\phi_i, \boldsymbol{\mu}_i, \boldsymbol{C}_i$ les paramètres de la gaussienne de la phase $i$.
L'algorithme de recalage est donc très similaire à celui de la corrélation globale.
Une linéarisation de la déformation de l'image pour des incréments $\delta\bphi$ combiné à la linéarisation du problème de minimisation revient à résoudre de manière itérative:
\begin{equation}
  \boldsymbol{M}^{(n)}\delta\bphi^{(n+1)} = \boldsymbol{A}^{(n)}
\end{equation}
avec
\begin{subequations}
  \begin{align}
    \boldsymbol{A}^{(n)} = \ & - \sum_{\x\in\Omega}
    \frac{ \partial\mathcal{L}^2\left(\fx,\fn^{(n)}\right) }{\partial\fn} \left(\boldsymbol{\nabla}\fn^{(n)} \otimes \bphi^{(n)}\cdot\x \right)\\
    \boldsymbol{M}^{(n)} = \ & \sum_{\x\in\Omega}
    \frac{ \partial^2\mathcal{L}^2\left(\fx,\fn^{(n)}\right) }{\partial\fn^2} \left(\boldsymbol{\nabla}\fn^{(n)} \otimes \bphi^{(n)}\cdot\x \right) \otimes \left(\boldsymbol{\nabla}\fn^{(n)} \otimes \bphi^{(n)}\cdot\x \right)
  \end{align}
\end{subequations}
où $\fn^{(n)}$ est la transformé de $\fn$ à l'itération $n$.

Grâce à l'identification des phases par les gaussiennes, les expressions de $\boldsymbol{A}$ et $\boldsymbol{M}$ sont analytiques et permettent une évaluation simple et rapide des expressions de l'algorithme de résolution (Newton-Raphson).
On peut même noter que dans le cadre de deux modalités identiques, les expressions théoriques des gaussiennes correspondantes permettent de retrouver le cadre classique de la corrélation d'image où $\mathcal{L}^2(f,g) = \frac{1}{2}(f-g)^2$.

\vs\par \figref{fig:2019-roubin-xn-checkerboard} montre le résultat du recalage entre les deux images $\fx$ et $\fn$ en les superposant à la manière d'un damier.
Ici, contrairement à \figref{fig:2019-roubin-xn}, on voit que les deux images sont spatialement commensurables, produisant ce que l'on peut maintenant considérer comme une seule image à deux champs de niveaux de gris $\boldsymbol{\varphi}(\x) = [\fx(\x), \fn(\x)]^t$.
Un des avantages, qui résulte directement de la méthode de résolution, est que chaque voxel est rattaché à une des phases identifiées par le fit des gaussiennes de l'histogramme joint \figref{fig:2019-roubin-xn-segmented}.
La morphologie est donc naturellement identifiée à l'issue du recalage et peut, par exemple, directement être utilisée pour une approche numérique à l'échelle mésoscopique.


\vs\par Couplée à de la corrélation d'image, cette méthode de recalage entre des images issues de tomographie à rayons X et neutrons nous a permis de caractériser les mouvements d'eau et les déformations dans une argile \cite{2020:stavropoulou:dynamics}.
