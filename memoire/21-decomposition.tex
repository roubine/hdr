\section{Analyses à l'échelle de la structure}
\label{sec:domain-decomposition}

\begin{sidenotes}
  \par Ce projet est l'objet de la thèse de Rida Grif intitulée ``Simulations multiéchelles à large amplitude pour les structures béton armé : apport des méthodes de décomposition de domaines à l'échelle mésoscopique'' commencée en octobre 2021.
  J'ai obtenu une dérogation afin d'en être le directeur de thèse.
  Ce projet et l'occasion d'une collaboration entre le laboratoire 3SR et le LaMcube de Lille (UMR 9013).
\end{sidenotes}

\subsection{Objectif et domaine d'application}

\par L'objectif de ce projet de thèse est de développer un cadre permettant de faire des simulations numériques d'éléments de structures du Génie civil en béton armé tout en prenant en compte explicitement les hétérogénéités du matériau.
Afin d'optimiser les ressources numériques nécessaires, une méthode de décomposition de domaines sera développée permettant de distribuer de multiples simulations concomitantes.

\begin{figure}[!ht]
  \centering
  \includegraphics[width=0.8\textwidth]{media/dd/website_dd}
  \caption{Algorithme de décomposition de domaine}
\end{figure}

\par Le cadre du projet est le développement de méthodes numériques permettant de rendre compte du comportement mécanique des matériaux cimentaires en modélisant leur structure à différentes échelles. Les échelles retenues sont:
\begin{itemize}
  \item  Pour l'échelle fine : l'échelle mésoscopique (de l'ordre du mm) pour laquelle sont modélisées les hétérogénéités du matériau (agrégats et macro-porosité) ainsi que des éléments d'armatures.
  \item Pour l'échelle macroscopique : l'échelle de la structure (de l'ordre de quelques décimètres) pour laquelle des sollicitations classiques d'éléments sont modélisées (poutres, poteaux ...)
\end{itemize}

\par La modélisation du comportement mécanique rentre dans le cadre des modèles mésoscopiques \cite{2006:wriggers:mesoscale} via l'utilisation d'une méthode Éléments Finis Enrichis adaptés aux matériaux hétérogènes quasi fragiles \cite{2015:roubin:multi-scale}.
Des campagnes expérimentales sous tomographie à RX ont permis de valider la pertinence de ces méthodes \cite{2018:stamati:tensile,2021:stamati:fracturing} dans un tel cadre.
Cependant, les ressources numériques nécessaires aux approches mésoscopiques contraignent les applications à des calculs à l'échelle du matériau.

\begin{figure}[!ht]
  \centering
  \includegraphics[width=0.9\textwidth]{media/dd/website_efem}
  \caption{Modèle mésoscopique utilisé \cite{2015:roubin:multi-scale}}
\end{figure}

\par Afin de pouvoir appliquer ce type de modèle à l'échelle de la structure, plusieurs approches sont envisageables :
\begin{itemize}
  \item Des méthodes de réduction de modèle : le comportement de la mésostructure est projeté sur une base de modes propres obtenus par la POD.
  \item Des méthodes de couplage fort entre les échelles : le comportement de la mésostructure est homogénéisé numériquement aux points d'intégration du calcul macroscopique (méthode des ``éléments finis au carré'').
  \item Des méthodes de décomposition de domaines : l'échelle macroscopique est ``découpée'' en plusieurs sous-domaines qui font l'objet de calculs mésoscopiques quasi indépendants. À l'échelle de la structure, un unique calcul macroscopique grossier est nécessaire, permettant de contrôler les conditions aux limites de chaque calcul fin. Ceux-ci peuvent ainsi être distribués sur des ressources différentes (CPU, RAM).
\end{itemize}

\par L'impact de la mésostructure sur le comportement des structures de Génie civil n'étant pas encore clairement identifié, c'est la méthode de décomposition de domaines qui est retenue dans ce projet. Contrairement aux deux autres approches présentées, elle conserve l'intégralité de la réponse à l'échelle fine au niveau de la structure, ce qui semble pertinent dans le cadre de la modélisation de propagations de fissures. Le développement de ces méthodes sera basé sur des algorithmes éprouvés depuis des décennies dans le cadre de l'élasticité linéaire et l'attention sera particulièrement portée sur le choix des fonctions d'interpolations entre les sous-domaines (comme les méthodes ``mortiers'') \cite{2001:wohlmuth:discretization}.
%
% \begin{figure}[!ht]
%   \centering
%   \includegraphics[width=0.5\textwidth]{media/dd/dd_preliminary}
%   \caption{Résultats préliminaires}
% \end{figure}

\subsection{Description}

% \par L'objectif de ce projet de thèse est de développer des méthodes de décomposition de domaines afin de pouvoir effectuer des calculs mésoscopiques rendant compte explicitement de la mésostructure et des éléments d'armature à l'échelle de la structure.

\par Le premier objectif est de travailler sur un verrou scientifique lié au couplage entre le modèle utilisé et les méthodes de décomposition de domaines.
Il n'a pas encore été montré dans la littérature que les modes de rupture des matériaux cimentaires (fissuration), et plus généralement que les comportements non linéaires adoucissants, sont modélisables avec ces méthodes de décomposition.
La problématique principale est de réussir à modéliser la propagation des fissures à travers plusieurs sous-domaines, sans modifications dues à la frontière.
Des études préliminaires ont montré des signes prometteurs si les fonctions d'interpolation des problèmes d'interface sont bien choisies (mortiers) \cite{2016:vallade:modelisation}.

\par Le deuxième objectif est d'appliquer ce cadre numérique à des calculs de structure, alliant ainsi, de façon innovante et directe, l'échelle mésoscopique et l'échelle de la structure. Des essais récents effectués sur de nouveaux types de renforts (câbles) pourront être une base de données expérimentales nécessaire à la validation du modèle \cite{2020:colyvas:behavior}.

\vs\par Dans le cadre de l'étude du comportement mécanique des matériaux cimentaires l'échelle mésoscopique porte une quantité importante d'informations liées à sa structure comme la géométrie et la topologie de la morphologie, les propriétés mécaniques de ses phases, les différents mécanismes de fissurations.
Les modèles de simulation dits ``mésoscopiques'' s'intéressent à représenter ces informations aux échelles fines (résolution de l'ordre du millimètre) afin de les prendre en compte explicitement.
Ainsi, ces modèles permettent d'adopter une démarche explicative afin d'analyser les conséquences de la mésostructure sur le comportement macroscopique du matériau qui émerge naturellement des simulations.

\par Les différentes phases matérielles de la mésostructure du matériau étant explicitement représentées (agrégats, porosité, pâte de ciment ...), la phénoménologie nécessaire pour représenter un comportement donné est donc nécessairement moins complexe que celle employée par des modèles macroscopiques.
Cependant, d'un point de vue technique, cet avantage est à mettre en opposition avec la complexité accrue de la méthode de calcul inhérente à la représentation de la mésostructure.

\par Parmi les problèmes rencontrés par ces approches, ce projet s'intéresse à réduire les coûts des simulations (le coût étant directement lié à la finesse de la discrétisation de la mésostructure) en adoptant des méthodes de décomposition de domaine. De telles approches permettraient, en réalisant des simulations plus performantes, de faire évoluer les problématiques qui peuvent être adressées vers deux échelles opposées : l'échelle de la structure (taille de l'ordre du mètre) et l'échelle microscopique (hétérogénéités de l'ordre du dixième de millimètre).

\par La mise en place de ces méthodes entraînera l'utilisation du supercalculateur régional (UMS Gricad, hébergé par l'UGA), voire national (Jean Zay).

\subsection{Méthode}
\par Plusieurs choix de méthodes sont nécessaires pour établir le cadre numérique de ce projet: a. le modèle morphologique nécessaire à la représentation des hétérogénéités de la mésostructure b. le modèle mécanique permettant de rendre compte de la fissuration du béton dans un cadre hétérogène c. la méthode de décomposition de domaines permettant d'effectuer les calculs à l'échelle de la structure.

\begin{itemize}
  \item Modèle morphologique: le modèle retenu est basé sur le principe d'excursions de champs aléatoires corrélés \cite{2015:roubin:meso-scale}. Il permet en effet de pouvoir assurer des conditions de continuité aux bords des sous-domaines et de générer des morphologies aléatoires à plusieurs phases et à formes non idéales (entraînant ainsi des concentrations de contraintes plus réalistes).
  \item Modèle mécanique : la méthode des Éléments Finis doublement Enrichis est retenue ici \cite{2015:roubin:multi-scale}. Développée par l'équipe encadrante depuis bientôt 10 ans, les récentes campagnes expérimentales ont montré la pertinence de son utilisation dans le cadre de la fissuration des bétons \cite{2021:stamati:fracturing}.
  \item Décomposition de domaines : la décomposition de domaines est à voir dans ce projet comme un moyen de communiquer entre l'échelle mésoscopique et l'échelle de la structure . Les différentes méthodes peuvent se regrouper en deux familles : les méthodes séquencées (communication unidirectionnelle de l'échelle macroscopique vers l'échelle fine) et les méthodes intégrées (communication dans les deux sens) \cite{2001:feyel:multi-scale}. C'est dans ce dernier cadre que le modèle est utilisé et le raccord entre les est effectué afin d'assurer la continuité des déplacements et des contraintes entre les sous-domaines (méthodes hybrides) \cite{2020:allix:non}.
\end{itemize}

\par Le développement du modèle mécanique a été pensé dans l'optique d'une stratégie multiéchelle et possède les moyens techniques de communication nécessaires à la décomposition de domaines (développement orienté composant, \cite{2008:kassiotis:cofeaps}).
En mode de production, ces méthodes nécessitent l'utilisation de supercalculateurs au moins de la taille des supercalculateurs régionaux (politique encouragée par le CNRS) pour pouvoir analyser l'extensibilité des calculs (scalabilité). L'Université Grenoble Alpes héberge le super calculateur de notre région (Gricad) dont l'accès est ouvert à ce genre de projets académiques. La possibilité de rejoindre le super calculateur national (Jean Zay) géré par la société civile GENCI (Grand Équipement National de Calcul Intensif) est également envisagée.

\begin{figure}[!ht]
  \centering
  \includegraphics[width=0.8\textwidth]{media/dd/website_gricad}
  \caption{Utilisation des calculateurs régionaux}
\end{figure}

\par Le résultat attendu est de lever le verrou scientifique relatif à la propagation de fissures macroscopiques en EF et en utilisant une méthode intégrée de décomposition de domaines puis,
d'appliquer la méthode à des éléments de structure en béton armé, jusqu'à la comparaison avec des essais issus de la littérature.


\begin{figure}[!ht]
  \centering
  \includegraphics[width=0.5\textwidth]{media/dd/dd_preliminary}
  \caption{Résultats préliminaires}
\end{figure}
